package arrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayListIterator {
    public static void main(String[] args) {
        List<Cliente> clientes = new ArrayList<>();
        Cliente luciano = new Cliente(1, "Luciano ");
        Cliente joao = new Cliente(2, "João");
        Cliente jessica = new Cliente(3, "Jessica ");
        Cliente gabriel = new Cliente(4, "Gabriel ");
        clientes.add(luciano);
        clientes.add(joao);
        clientes.add(jessica);
        clientes.add(gabriel);
        // utilizando for normal 
        for (int i = 0; i < clientes.size(); i++) {
            System.out.println("IMPRESSO UTILIZANDO FOR NORMAL " + clientes.get(i));
        }

        // Iterator somente para frente
        Iterator<Cliente> clienteIterator = clientes.iterator();
        while (clienteIterator.hasNext()) {
            Cliente meuCliente = clienteIterator.next();
            System.out.println("cliente do iterator " + meuCliente);
            // clienteIterator.remove(); // só pode ser executado depois de next
        }

        for (Cliente cliente : clientes) {
            System.out.println(cliente);
        }

        // ListIterator --> qualquer ordem
        ListIterator<Cliente> clientesListIterator = clientes.listIterator(clientes.size());
        while (clientesListIterator.hasPrevious()) {
            Cliente cli = clientesListIterator.previous();
            System.out.println("clientes do ListIterator do size() até 0 " + cli);
        }

        ListIterator<Cliente> clientesListIterator2 = clientes.listIterator(0);
        while (clientesListIterator2.hasNext()) {
            Cliente cli = clientesListIterator2.next();
            System.out.println("clientes do ListIterator do 0 até size()  " + cli);
        }
    }
}
