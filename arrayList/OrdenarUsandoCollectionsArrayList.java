package arrayList;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class OrdenarUsandoCollectionsArrayList {
    public static void main(String[] args) {
        List<Cliente> clientes = new ArrayList<>();
        Cliente luciano = new Cliente(1, "Luciano ");
        Cliente joao = new Cliente(2, "João");
        Cliente jessica = new Cliente(3, "Jessica ");
        Cliente gabriel = new Cliente(4, "Gabriel ");
        clientes.add(luciano);
        clientes.add(joao);
        clientes.add(jessica);
        clientes.add(gabriel);

        Collections.shuffle(clientes); // embararalha os itens
        System.out.println("IMPRIMIR EMBARALHANDO ");
        Iterator<Cliente> clientesIterator = clientes.listIterator(0);
        while (clientesIterator.hasNext()) {
            System.out.println(clientesIterator.next());
        }

        // Ordenar os items
        Collections.sort(clientes, new Comparator<Cliente>() {
            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.nome.compareTo(o2.nome);
            }
        });

        System.out.println("IMPRIMIR ORDENANDO POR NOME");
        Iterator<Cliente> clientesIterator2 = clientes.listIterator(0);
        while (clientesIterator2.hasNext()) {
            System.out.println(clientesIterator2.next());
        }

        // Ordenar os items
        Collections.sort(clientes, new Comparator<Cliente>() {
            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.id.compareTo(o2.id);
            }
        });

        System.out.println("IMPRIMIR ORDENANDO POR CODIGO ");
        Iterator<Cliente> clientesIterator3 = clientes.listIterator(0);
        while (clientesIterator3.hasNext()) {
            System.out.println(clientesIterator3.next());
        }
    }
}
