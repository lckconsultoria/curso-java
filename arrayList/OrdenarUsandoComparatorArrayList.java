package arrayList;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class OrdenarUsandoComparatorArrayList {
    public static void main(String[] args) {
        List<Cliente> clientes = new ArrayList<>();
        Cliente luciano = new Cliente(1, "Luciano ");
        Cliente joao = new Cliente(2, "João");
        Cliente jessica = new Cliente(3, "Jessica ");
        Cliente gabriel = new Cliente(4, "Gabriel ");
        clientes.add(luciano);
        clientes.add(joao);
        clientes.add(jessica);
        clientes.add(gabriel);

        Collections.shuffle(clientes); // embararalha os itens
        System.out.println("IMPRIMIR EMBARALHANDO ");
        Iterator<Cliente> clientesIterator = clientes.listIterator(0);
        while (clientesIterator.hasNext()) {
            System.out.println(clientesIterator.next());
        }
        
        System.out.println("CLIENTE ORDENADO POR NOME ");
        clientes.sort(new Comparator<Cliente>() {
            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.nome.compareToIgnoreCase(o2.nome);
            }
        });

        for (Cliente cliente : clientes) {
            System.out.println(cliente);
        }

        
        
        System.out.println("CLIENTE ORDENADO POR CODIGO ");
        clientes.sort(new Comparator<Cliente>() {
            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.id.compareTo(o2.id);
            }
        });

        for (Cliente cliente : clientes) {
            System.out.println(cliente);
        }
    }
}
