package arrayList;

import java.util.Arrays;

public class TestePerformance {
    private static final int TOTAL_ITEMS = 1000_000_000;
    private static final int IDX_EXCLUIR = 500_000_000;

    public static void main(String[] args) {
        System.out.println("Iniciando programa");
        long start = 0;
        start = System.currentTimeMillis();
        System.out.println("Iniciando alocação do array principal");
        Integer[] myBigArray = new Integer[TOTAL_ITEMS];
        System.out.printf("Instanciou o array  : %d secs %n ", (System.currentTimeMillis() - start) / 1000);

        System.out.println("Iniciando fill no array ");
        start = System.currentTimeMillis();
        Arrays.fill(myBigArray, 0);
        System.out.printf("preencheu o array : %d secs %n ", (System.currentTimeMillis() - start) / 1000);

        System.out.println("Iniciando instanciação do array tmpArray ");
        start = System.currentTimeMillis();
        Integer[] tmpArray = new Integer[myBigArray.length - 1];
        System.out.printf("Instanciou  o novo array tmpArray de size()-1  : %d secs %n ",
                (System.currentTimeMillis() - start) / 1000);

        start = System.currentTimeMillis();
        int newIdx = 0;
        for (int i = 0; i < myBigArray.length; i++) {
            if (i == IDX_EXCLUIR) {
                continue;
            }
            tmpArray[newIdx++] = myBigArray[i];
        }
        myBigArray = tmpArray;
        System.out.printf("Fim de exclusão manual : %d secs %n", (System.currentTimeMillis() - start) / 1000);

        // utilizando o copy
        myBigArray = new Integer[TOTAL_ITEMS];
        Arrays.fill(myBigArray, 0);
        System.out.println("*".repeat(40) + " iniciando remoçao com System.arraycopy");
        tmpArray = new Integer[myBigArray.length - 1];
        start = System.currentTimeMillis();
        System.arraycopy(myBigArray, 0, tmpArray, 0, IDX_EXCLUIR);
        System.arraycopy(myBigArray, IDX_EXCLUIR + 1, tmpArray, IDX_EXCLUIR, tmpArray.length - IDX_EXCLUIR);
        System.out.printf("Fim de exclusão System.arraycopy : %d secs %n", (System.currentTimeMillis() - start) / 1000);

    }
}
