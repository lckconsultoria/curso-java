package arrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsarArrayList {
    public static void main(String[] args) {
        // List<Integer> listInteger1 = new ArrayList<>();
        // listInteger1.add(1);
        // listInteger1.add(2);
        // listInteger1.add(3);
        // listInteger1.add(1, 55);
        // listInteger1.set(0, 99);
        // if (listInteger1.contains(88)) {
        // System.out.println("contem ");
        // } else {
        // System.out.println("nao contem ");
        // }

        List<Cliente> clientes = new ArrayList<>();
        Cliente luciano = new Cliente(1, "Luciano ");
        Cliente joao = new Cliente(2, "João");
        Cliente jessica = new Cliente(3, "Jessica ");
        Cliente gabriel = new Cliente(4, "Gabriel ");
        clientes.add(luciano);
        clientes.add(joao);
        clientes.add(jessica);
        clientes.add(gabriel);

        clientes.add(new Cliente(4, "Robson"));
        var felipe = new Cliente(5, "Felipe");
        clientes.add(felipe);

        for (Cliente cliente : clientes) {
            System.out.println(cliente);
        }

        Cliente cliente4NaoInclusoNaListaParaTestes = new Cliente(4, "Código 4 para testes");
        if (clientes.contains(cliente4NaoInclusoNaListaParaTestes)) {
            System.out.println("contem ");
        } else {
            System.out.println("NAO contem ");
        }

        int idxCliente2 = clientes.indexOf(cliente4NaoInclusoNaListaParaTestes);
        System.out.println(idxCliente2);
        System.out.println(clientes.get(idxCliente2));

        int lastIdxCliente2 =
        clientes.lastIndexOf(cliente4NaoInclusoNaListaParaTestes);
        System.out.println(lastIdxCliente2);
        System.out.println(clientes.get(lastIdxCliente2));

        Cliente cliRemovido = clientes.remove(2);
        System.out.println("Cliente removido " + cliRemovido);
        for (Cliente cliente : clientes) {
            System.out.println(cliente);
        }

        System.out.println(
                clientes.remove(cliente4NaoInclusoNaListaParaTestes) ? "Cliente removido" : "Cliente nao removido ");
        System.out.println(
                clientes.remove(cliente4NaoInclusoNaListaParaTestes) ? "Cliente removido" : "Cliente nao removido ");
        System.out.println(
                clientes.remove(cliente4NaoInclusoNaListaParaTestes) ? "Cliente removido" : "Cliente nao removido ");

        List<Cliente> clientesAseremExcluidos = new ArrayList<>();
        clientesAseremExcluidos.add(joao);
        clientesAseremExcluidos.add(felipe);
        clientes.removeAll(clientesAseremExcluidos);
        System.out.println("apos remover lista ");
        for (Cliente cliente : clientes) {
            System.out.println("Cliente que ficou " + cliente);
        }

        clientesAseremExcluidos.removeIf(cli -> cli.nome.equals("João"));
        for (Cliente cliente : clientesAseremExcluidos) {
            System.out.println("cliente que ficou apos removeIf " + cliente);
        }

        System.out.println("ArrayList from Array ");
        // var listaInserir = Arrays.asList(gabriel, jessica, joao, felipe, new Cliente(50, "Yuri"));

        // List<Cliente> clientesFromArray =  new ArrayList<>();
        // clientesFromArray =  Arrays.asList(gabriel, jessica, joao, felipe, new Cliente(50, "Yuri"));

        List<Cliente> clientesFromArray =  Arrays.asList(gabriel, jessica, joao, felipe, new Cliente(50, "Yuri"));
        for (Cliente cliente : clientesFromArray) {
            System.out.println("Cliente adicionado com Arrays.asList "+  cliente);
        }

    }
}
