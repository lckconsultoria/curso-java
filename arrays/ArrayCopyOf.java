package arrays;

import java.util.Arrays;
public class ArrayCopyOf {
    
    public static void main(String[] args) {
        Integer[] meuArray = new Integer[] {1,2,3,4,5,6,7,8,9,10}; 
       meuArray = Arrays.copyOf(meuArray, 9);
       System.out.println(Arrays.toString(meuArray));
       
       meuArray = Arrays.copyOf(meuArray, 20);
       System.out.println(Arrays.toString(meuArray));
    }
}
