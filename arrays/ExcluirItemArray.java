package arrays;

import java.util.Arrays;

public class ExcluirItemArray {
    public static void main(String[] args) {
        int[] inteiros1 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        var newInt1 = maneira1(inteiros1, 3);
        for (int i : newInt1) {
            System.out.println(i);
        }

        int[] inteiros2 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        var newInt2 = maneira2(inteiros2, 3);
        for (int i : newInt2) {
            System.out.println(i);
        }

    }

    static int[] maneira1(int[] inteiros, int indiceExcluir) {
        if (indiceExcluir > inteiros.length) {
            throw new IllegalArgumentException("não pode excluir indice maior que 9");
        }

        int[] tmpInteiros = new int[inteiros.length - 1];
        int newIndice = 0;
        for (int i = 0; i < inteiros.length; i++) {
            if (i == indiceExcluir) {
                continue;
            }
            tmpInteiros[newIndice++] = inteiros[i];
        }
        inteiros = tmpInteiros;
        return inteiros;
    }
    

    static int[] maneira2(int[] inteiros, int indiceExcluir) {
        if (indiceExcluir > inteiros.length) {
            throw new IllegalArgumentException("não pode excluir indice maior que 9");
        }
        int[] tmpInt = new int[inteiros.length - 1];

        System.arraycopy(inteiros, 0, tmpInt, 0, indiceExcluir); // copia até antes do indice para excluir
        System.out.println(Arrays.toString(tmpInt));
        System.arraycopy(inteiros, indiceExcluir + 1, tmpInt, indiceExcluir, tmpInt.length - indiceExcluir);
        // copia
        // após o
        // indice a
        // excluir
        return tmpInt;
    }

}
