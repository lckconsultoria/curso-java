package arrays;

import java.util.Arrays;

public class MudarTamanhoArray {
    public static void main(String[] args) {
        int[] inteiros = new int[0];

        // inicializei com i em cada posição
        for (int i = 0; i < 10; i++) {
            inteiros[i] = i;
        }
        
        // aumentei o tamanho do array em 1 
        inteiros = Arrays.copyOf(inteiros, inteiros.length + 1);
        for (int i : inteiros) {
            System.out.println(i);
        }

        inteiros = Arrays.copyOf(inteiros, 2); // só deixa os primeiros 2 
        System.out.println(Arrays.toString(inteiros)); 
    }
}
