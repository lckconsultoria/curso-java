package arrays;

import java.util.Arrays;

public class UsandoArray {
    public static void main(String[] args) {
        Carro carros[] = new Carro[3];
        Carro carro1 = new Carro();
        carro1.id = 1;
        carro1.descricao = "fusca";
        carros[0] = carro1;

        Carro carro2 = new Carro();
        carro2.id = 2;
        carro2.descricao = "gol";
        carros[1] = carro2;

        Carro carro3 = new Carro();
        carro3.id = 3;
        carro3.descricao = "bmw";
        carros[2] = carro3;
        for (Carro carro : carros) {
            System.out.println(carro);
        }
        System.out.println(carros[2]);
        System.out.println(Arrays.toString(carros));    // imprime [Carro [id=1, descricao=fusca], Carro [id=2, descricao=gol], Carro [id=3, descricao=bmw]]

        // System.out.println(carros[3]); // java.lang.ArrayIndexOutOfBoundsException
    }
}
