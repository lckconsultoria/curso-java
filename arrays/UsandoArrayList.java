package arrays;
import java.util.ArrayList;
import java.util.List;

public class UsandoArrayList {
    public static void main(String[] args) {
        List<Carro> carros = new ArrayList<>(); // declara e inicializa uma List de Carros 
        Carro carro1 = new Carro();
        carro1.id = 1;
        carro1.descricao = "fusca";
        carros.add(carro1);
        
        Carro carro2 = new Carro();
        carro2.id = 2;
        carro2.descricao = "gol";
        carros.add(carro2);
        
        Carro carro3 = new Carro();
        carro3.id = 3;
        carro3.descricao = "bmw";
        carros.add(carro3);
        for (Carro carro : carros) {
            System.out.println(carro);
        }
    }
}
