package arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class UsandoArrayListIterator {
    public static void main(String[] args) {
        List<Carro> carros = new ArrayList<>(); // declara e inicializa uma List de Carros
        Carro carro1 = new Carro();
        carro1.id = 1;
        carro1.descricao = "fusca";
        carros.add(carro1);

        Carro carro2 = new Carro();
        carro2.id = 2;
        carro2.descricao = "gol";
        carros.add(carro2);

        Carro carro3 = new Carro();
        carro3.id = 3;
        carro3.descricao = "bmw";
        carros.add(carro3);

        ListIterator<Carro> carroListIterator = carros.listIterator(); // obtem um listIterator
        while (carroListIterator.hasNext()) { // tem próximo
            Carro carro = carroListIterator.next(); // pega o item e avança para o próximo
            System.out.println(carro);
        }

        while (carroListIterator.hasPrevious()) { // tem anterior
            Carro carro = carroListIterator.previous(); // pega o item e avança para o item anterior
            System.out.println(carro);
        }
    }
}
