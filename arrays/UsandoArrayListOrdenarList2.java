package arrays;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UsandoArrayListOrdenarList2 {
    public static void main(String[] args) {
        List<Carro> carros = new ArrayList<>(); // declara e inicializa uma List de Carros
        Carro carro1 = new Carro();
        carro1.id = 1;
        carro1.descricao = "fusca";
        carros.add(carro1);

        Carro carro2 = new Carro();
        carro2.id = 2;
        carro2.descricao = "gol";
        carros.add(carro2);

        Carro carro3 = new Carro();
        carro3.id = 3;
        carro3.descricao = "bmw";
        carros.add(carro3);

        // Ordena uma lista direto pelo lista
        carros.sort(new Comparator<Carro>() {
            @Override
            public int compare(Carro c1, Carro c2) {
                return c1.descricao.compareToIgnoreCase(c2.descricao);
            }
        });
        
        System.out.println(carros);
    }
}
