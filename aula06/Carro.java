public class Carro {
    String placa;
    Fabricante fabricante;
    String modelo;
    String cor;
    int anoFabricacao;
    boolean ativo = true;
    double valorVeiculo;

    static double calculaSeguro() {
        return 1000 * .10;
    }

    void setModelo(String modelo) {
        this.modelo = modelo;
    }

    void ativar() {
        ativo = true;
    }

    void inativar() {
        System.out.println("enviando email para dono da frota ");
        ativo = false;
    }

    @Override
    public String toString() {
        return "Carro [placa=" + placa + ", fabricante=" + fabricante + ", modelo=" + modelo + ", cor=" + cor
                + ", anoFabricacao=" + anoFabricacao + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((placa == null) ? 0 : placa.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Carro other = (Carro) obj;
        if (placa == null) {
            if (other.placa != null)
                return false;
        } else if (!placa.equals(other.placa))
            return false;
        return true;
    }

    
}
