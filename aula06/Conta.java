public class Conta {
    double saldo = 0;

    double pegaSaldo() {
        return saldo;
    }

    void atribuiSaldo(double valorAtribuir) {
        saldo = valorAtribuir;
    }

}
