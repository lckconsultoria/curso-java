public class ContaRefatorada {
    double saldo = 0;

    double pegaSaldo() {
        return saldo;
    }

    void depositar(double valorDepositar) {
        saldo += valorDepositar;
    }

    void sacar(double valorSacar) {
        saldo -= valorSacar;
    }

}
