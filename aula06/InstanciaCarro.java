public class InstanciaCarro {
    public static void main(String[] args) {

        Fabricante fiat = new Fabricante();
        fiat.id = 1;
        fiat.nome = "fiat";


        Carro meuCarro = new Carro();
        meuCarro.placa = "BMQ-5031";
        meuCarro.cor = "vermelho";
        meuCarro.anoFabricacao = 2013;
        meuCarro.fabricante = fiat;
        meuCarro.modelo = "GrandSiena";
        meuCarro.inativar();
        
        Fabricante volksWagen = new Fabricante();
        Carro carroGabriel = new Carro();
        carroGabriel.placa = "BMQ-5234";
        carroGabriel.cor = "preto";
        carroGabriel.anoFabricacao = 2021;
        carroGabriel.fabricante = volksWagen;
        carroGabriel.modelo = "carrao";

        System.out.println(meuCarro);
        System.out.println(carroGabriel);

    }
}
