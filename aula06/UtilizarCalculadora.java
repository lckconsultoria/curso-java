public class UtilizarCalculadora {
    public static void main(String[] args) {
        Calculadora calculadoraObjeto = new Calculadora();
        System.out.println(calculadoraObjeto.adicao(5, 6));
        System.out.println(calculadoraObjeto.subtracao(5, 6));
        System.out.println(calculadoraObjeto.divisao(5, 6));
        System.out.println(calculadoraObjeto.multiplicacao(5, 6));

        // System.out.println(calculadoraObjeto.divisao(calculadoraObjeto.adicao(5, 3),2) );
       System.out.println(calculadoraObjeto.divisao(10, 0));
    }
}
