
package membrostatic;

public class ThisExample {

    public static void main(String[] args) {
        Carro carro = new Carro();
        carro.modelo = "grand Sienna";
        carro.valorVeiculo = 10000;
        // carro.exibeThis();
        System.out.println(Carro.calculaSeguro(carro));

        // carro.setModelo("fiat uno ");
        // System.out.println(carro.modelo);
       ThisExample thisExample = new ThisExample();
       thisExample.imprimirAloMundo(); 
    }

    void imprimirAloMundo() {
        System.out.println("alo mundo ");
    }
}
