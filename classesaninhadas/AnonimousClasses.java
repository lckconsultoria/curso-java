package classesaninhadas;

public class AnonimousClasses {
    
    public static void main(String[] args) {
        NotificadorClienteInterface notificarPorSMS = new NotificadorClienteInterface() {

            @Override
            public void enviaMensagem(String mensagem) {
                System.out.println("Notificou por sms "+mensagem);
            }
        };

        notificarPorSMS.enviaMensagem("mensagem teste ");

        Cliente cli = new Cliente(10L, "Luciano ") {
            @Override
            void activate() {
                System.out.println("antes activate");
                super.activate();
                System.out.println("depois activate");
            }
            
            @Override
            void deActivate() {
                System.out.println("antes deActivate");
                super.deActivate();
                System.out.println("depois deActivate");
            }
        };
        cli.activate();
        cli.deActivate();
    }
}
