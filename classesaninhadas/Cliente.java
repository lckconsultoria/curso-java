package classesaninhadas;

public class Cliente {
    private Long id;
    private String nome;
    private StatusEnum status;

    public Cliente(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    void activate() {
        status = StatusEnum.ATIVO;
    }

    void deActivate() {
        status = StatusEnum.INATIVO;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public static enum StatusEnum {
        ATIVO,
        INATIVO
    }
}
