package classesaninhadas;

public class LocalNestedClasses {

    public static void main(String[] args) {
        Integer integer1 = 10;
        if (integer1 == 10) {
            class MinhaClasseLocal {
                Long id;
                String name;

                public MinhaClasseLocal(Long id, String name) {
                    this.id = id;
                    this.name = name;
                }

                void imprimirDentroDaClasseLocal() {
                    System.out.println(integer1);
                }
            }

            MinhaClasseLocal minhaClasseLocal = new MinhaClasseLocal(1L, "Luciano");
            System.out.println(minhaClasseLocal);
            minhaClasseLocal.imprimirDentroDaClasseLocal();
        }
        // MinhaClasseLocal // não existe fora do bloco
    }
}
