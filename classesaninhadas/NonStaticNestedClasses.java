package classesaninhadas;

import java.util.ArrayList;
import java.util.List;

public class NonStaticNestedClasses {
    private Long idPedido;
    private String cliente;
    private List<Item> items = new ArrayList<>();

    
    private void metodoDaClassePrincipal() {
        System.out.println("metodo da classe principal não static e private ");
    }

    private class Item {
        private String produto;
        private Integer quantidade;

        private void metodoClasseItem() {
            metodoDaClassePrincipal();
        }
    }

    

}
