package classesaninhadas;

import java.util.ArrayList;
import java.util.List;

public class NonStaticNestedClassesShadowing {
    private Long id;
    private String cliente;
    private List<Item> items = new ArrayList<>();

    
    private void metodoDaClassePrincipal() {
        System.out.println("metodo da classe principal não static e private ");
    }

    private class Item {
        private Long id;
        private String produto;
        private Integer quantidade;

        private void metodoClasseItem() {
           System.out.println(NonStaticNestedClassesShadowing.this.id );

        }
    }
}
