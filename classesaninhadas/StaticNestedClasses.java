package classesaninhadas;

import java.util.ArrayList;
import java.util.List;

public class StaticNestedClasses {
    private Long idPedido;
    private String cliente;
    private List<Item> items = new ArrayList<>();

    private static void acessadoPelaClasseStatic() {
        System.out.println("imprimiu ");
        
    }

    private static void acessandoMembrosDaClasseStatic() {
        Item item = new Item();
        System.out.println(item.produto);
        System.out.println(item.quantidade);
        item.metodoClasseItem();

        
    }
    private static class Item {
        private String produto;
        private Integer quantidade;

        private void metodoClasseItem() {
            // System.out.println(idPedido + " - " + cliente);  // não consegue acessar dados da classe principal
            acessadoPelaClasseStatic();
        }
    }
}
