package construtores;

public class ConstrutorEncadeado {
    Integer id;
    String nome;

    public ConstrutorEncadeado() {
        // System.out.println("nao compila pois o this tem de ser a primeira
        // instruçao");
        this(0, "Nome padrão");
    }

    public ConstrutorEncadeado(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public ConstrutorEncadeado(Carro carro) {
        this.id = carro.id;
        this.nome = carro.descricao;
    }
}
