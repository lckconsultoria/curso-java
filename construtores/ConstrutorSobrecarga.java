package construtores;

public class ConstrutorSobrecarga {
    Integer id;
    String nome;

    public ConstrutorSobrecarga() {
        // System.out.println("nao compila pois o this tem de ser a primeira
        // instruçao");
        this(0, "Nome padrão");
    }

    public ConstrutorSobrecarga(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public ConstrutorSobrecarga(Carro carro) {
        this.id = carro.id;
        this.nome = carro.descricao;
    }
}
