
package construtores;

public class ConstrutorVariaveisFinal {
    Integer id;
    String nome;
    final Pessoa pessoa = new Pessoa(); // ou inicializa na declaração
    final Carro carro;

    public ConstrutorVariaveisFinal(Carro carro) { // ou inicializa no construtor
        this.carro = carro;
    }
}
