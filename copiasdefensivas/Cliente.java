public class Cliente {
    int id;
    String primeiroNome;
    String sobreNome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public NomeCompleto getNomeCompleto() {
        return NomeCompleto.create(this.primeiroNome, this.sobreNome);
    }

}

final class NomeCompleto {
    private String nome;
    private String sobreNome;

    private NomeCompleto(String nome, String sobreNome) {
        this.nome = nome;
        this.sobreNome = sobreNome;
    }

    public static NomeCompleto create(String nome, String sobrenome) {
        return new NomeCompleto(nome, sobrenome);
    }

    public String getNome() {
        return this.nome;
    }

    public String getSobreNome() {
        return this.sobreNome;
    }
}