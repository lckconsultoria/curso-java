public record HorarioRecordCompactConstructor(int hora, int minuto) {
    
    // tem de ser na mesma sequencia da declaração 
    public HorarioRecordCompactConstructor {
        // faz validações 
        // não é necessario atribuir para this
    }
}
