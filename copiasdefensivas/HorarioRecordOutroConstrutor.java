public record HorarioRecordOutroConstrutor(int hora, int minuto) {
    
    // tem de ser na mesma sequencia da declaração 
    public HorarioRecordOutroConstrutor(int hora, int minuto) {
        // faz validações 
        this.hora = hora;
        this.minuto = minuto;
    }
}
