package designpatterns.comportamental.strategy;

public interface LckPixInterface {
    void enviarPix(double valor);
    void consultarPix();
}