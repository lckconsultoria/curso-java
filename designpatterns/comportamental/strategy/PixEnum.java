package designpatterns.comportamental.strategy;

public enum PixEnum {
    PIX_PDV(new PixPdv()), 
    PIX_SICOB(new PixSicob()), 
    PIX_ITAU(new PixItau()), 
    PIX_SICRED(new PixSicred());

    private LckPixInterface implementacao;

    private PixEnum(LckPixInterface implementacao) {
        this.implementacao = implementacao;
    }

    public LckPixInterface getImplementacao() {
        return this.implementacao;
    }
}
