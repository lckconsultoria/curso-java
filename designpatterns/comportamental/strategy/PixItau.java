package designpatterns.comportamental.strategy;

public class PixItau implements LckPixInterface {

    @Override
    public void enviarPix(double valor) {
        System.out.println("PixITAU enviado com sucesso");
    }

    @Override
    public void consultarPix() {
        System.out.println("PixITAU consultado com sucesso aprovado");
    }
}
