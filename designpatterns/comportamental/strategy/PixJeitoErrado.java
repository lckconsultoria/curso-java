package designpatterns.comportamental.strategy;

public class PixJeitoErrado {

    public void envia(double valor, PixEnum banco) {
        if (banco.equals(PixEnum.PIX_ITAU)) {
            System.out.println(" envio itau");
        } else if (banco.equals(PixEnum.PIX_PDV)) {
            System.out.println(" envio PIXPDV");
        } else if (banco.equals(PixEnum.PIX_ITAU)) {
            System.out.println(" envio itau");
        } else if (banco.equals(PixEnum.PIX_SICRED)) {
            System.out.println(" envio pixSicred");
        } else {
            throw new IllegalArgumentException(" banco nao implemtado ");
        }
    }

    public void consulta(PixEnum banco) {
        if (banco.equals(PixEnum.PIX_ITAU)) {
            System.out.println(" consulta itau");
        } else if (banco.equals(PixEnum.PIX_PDV)) {
            System.out.println(" consulta PIXPDV");
        } else if (banco.equals(PixEnum.PIX_ITAU)) {
            System.out.println(" consulta itau");
        } else if (banco.equals(PixEnum.PIX_SICRED)) {
            System.out.println(" consulta pixSicred");
        } else {
            throw new IllegalArgumentException(" banco nao implemtado ");
        }
    }

    public static void main(String[] args) {
        PixJeitoErrado pix = new PixJeitoErrado();
        pix.envia(100.00, PixEnum.PIX_ITAU);
    }
}
