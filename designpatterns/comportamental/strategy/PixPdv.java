package designpatterns.comportamental.strategy;

public class PixPdv implements LckPixInterface {

    @Override
    public void enviarPix(double valor) {
        System.out.println("pixpdv enviado com sucesso");
    }

    @Override
    public void consultarPix() {
        System.out.println("pixpdv consultado com sucesso aprovado");
    }
    
}
