package designpatterns.comportamental.strategy;

public class PixSicob implements LckPixInterface {
    
    @Override
    public void enviarPix(double valor) {
        System.out.println("SICOB enviado com sucesso");
    }

    @Override
    public void consultarPix() {
        System.out.println("SICOB consultado com sucesso aprovado");
    }
}
