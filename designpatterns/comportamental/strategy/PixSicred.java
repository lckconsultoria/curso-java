package designpatterns.comportamental.strategy;

public class PixSicred implements LckPixInterface {
    @Override
    public void enviarPix(double valor) {
        System.out.println("PixSicred enviado com sucesso");
    }

    @Override
    public void consultarPix() {
        System.out.println("PixSicred consultado com sucesso aprovado");
    }
    
}
