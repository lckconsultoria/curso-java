package designpatterns.comportamental.strategy;

public class ProcessaPix {
    public static void main(String[] args) {

        PixSicob pixSicob = new PixSicob();
        processa(pixSicob);

        LckPixInterface pixPdv = new PixPdv();
        processa(pixPdv);

        LckPixInterface implementacaoPixPdv = PixEnum.PIX_PDV.getImplementacao();
        implementacaoPixPdv.enviarPix(100.00);
        implementacaoPixPdv.consultarPix();

        processaPorEnum(PixEnum.PIX_PDV);
        processaPorEnum(PixEnum.PIX_SICRED);
    }

    static void processa(LckPixInterface pix) {
        pix.enviarPix(100.0);
        pix.consultarPix();
    }

    static void processaPorEnum(PixEnum pixEnum) {
        pixEnum.getImplementacao().enviarPix(100.0);
        pixEnum.getImplementacao().consultarPix();
    }
}
