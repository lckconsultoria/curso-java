package builder;

import java.time.LocalDate;

public class Cliente {
    private Long id;
    private String nome;
    private LocalDate nascimento;
    private boolean ativo;

    // public Cliente() {
    // }

    // public Cliente(Long id, String nome, LocalDate nascimento) {
    // this.id = id;
    // this.nome = nome;
    // this.nascimento = nascimento;

    // }

    public void activate() {
        ativo = true;
    }
    public void deActivate() {
        ativo = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cliente other = (Cliente) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Cliente [id=" + id + ", nome=" + nome + ", nascimento=" + nascimento + "]";
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    private static void metodoStaticNaClassePrincipal() {
        System.out.println(" imprimiu qualquer coisa ");
    }

    public void metodoNaoStaticNaClassePrincipal() {
        System.out.println(" imprimiu qualquer coisa ");
        // consegue acessar todos os membros da classe statica mesmo private...
        // bastando para isso criar um objeto do tipo da classe static
        builder b = new builder();
        b.build();

    }

    // padrao builder
    public static class builder {

        private static Cliente instance;

        public builder() {
            instance = new Cliente();
        }

        public Cliente build() {
            metodoStaticNaClassePrincipal();
            if (instance.getNascimento() == null) {
                throw new RuntimeException("Precisa da data do nascimento");
            }
            return instance;
        }

        public builder id(Long id) {
            instance.setId(id);
            return this;
        }

        public builder nome(String nome) {
            instance.setNome(nome);
            return this;
        }

        public builder nascimento(LocalDate nascimento) {
            instance.setNascimento(nascimento);
            return this;
        }
    }

}
