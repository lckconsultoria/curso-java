package builder;

import java.time.LocalDate;

public class ClienteBuilder {
    private static Cliente instance;

    public ClienteBuilder() {
        instance = new Cliente();
    }

    public Cliente build() {
        if (instance.getNascimento() == null) {
            throw new RuntimeException("Precisa da data do nascimento");
        }
        return instance;
    }

    public ClienteBuilder id(Long id) {
        instance.setId(id);
        return this;
    }

    public ClienteBuilder nome(String nome) {
        instance.setNome(nome);
        return this;
    }

    public ClienteBuilder nascimento(LocalDate nascimento) {
        instance.setNascimento(nascimento);
        return this;
    }

}
