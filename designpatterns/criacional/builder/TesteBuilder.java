package builder;

import java.time.LocalDate;

public class TesteBuilder {

    public static void main(String[] args) {
        Cliente cliente = new Cliente.builder()
                .id(10L)
                .nome("Luciano")
                .nascimento(LocalDate.now())
                .build();

        System.out.println(cliente);

        Cliente cliente2 = new ClienteBuilder()
                .id(50L)
                .nome("Andre Nobre")
                .nascimento(LocalDate.now())
                .build();
        System.out.println(cliente2);
    }
}
