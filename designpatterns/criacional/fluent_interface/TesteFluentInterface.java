package fluent_interface;

import java.time.LocalDate;

public class TesteFluentInterface {
    public static void main(String[] args) {

        // Sem utilizar o fluent Interface
        // Cliente cliente = new Cliente();
        // cliente.setId(10L);
        // cliente.setNome("Luciano");
        // cliente.setNascimento(LocalDate.of(1971, 11, 17) );
        // System.out.println(cliente);

        // // utilizando o fluent interface
        Cliente cliente = new Cliente();
        cliente.id(1L)
                .nome("Luciano")
                .nascimento(LocalDate.now());
        System.out.println(cliente);
    }
}
