package singleton;

public class Configuration {
    private Long id;
    private SimNaoEnum utilizaVendaPrazo;
    private Double percentualJuros;
    private Integer diasCobrancaJuros;

    public Configuration(Long id, SimNaoEnum utilizaVendaPrazo, Double percentualJuros, Integer diasCobrancaJuros) {
        this.id = id;
        this.utilizaVendaPrazo = utilizaVendaPrazo;
        this.percentualJuros = percentualJuros;
        this.diasCobrancaJuros = diasCobrancaJuros;
    }

    public Long getId() {
        return id;
    }

    public SimNaoEnum getUtilizaVendaPrazo() {
        return utilizaVendaPrazo;
    }

    public Double getPercentualJuros() {
        return percentualJuros;
    }

    public Integer getDiasCobrancaJuros() {
        return diasCobrancaJuros;
    }

    
    @Override
    public String toString() {
        return "Configuration [id=" + id + ", utilizaVendaPrazo=" + utilizaVendaPrazo + ", percentualJuros="
                + percentualJuros + ", diasCobrancaJuros=" + diasCobrancaJuros + "]";
    }

  
    public void setPercentualJuros(Double percentualJuros) {
        this.percentualJuros = percentualJuros;
    }
}
