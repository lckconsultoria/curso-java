package singleton;

// EAGER SINGLETON (ANSIOSO)
// JA OCUPA A MEMORIA DO OBJETO DESDEQUE É CRIADO 
// COM 2 CLASSES, NORMALMENTE SE EU NAO TIVESSE ACESSO A CLASSE "Configuration" e precise fazer um singleton dela
public class ConfigurationSingletonEagerDuasClasses {
    private ConfigurationSingletonEagerDuasClasses() {
    }

    private static Configuration instance = new Configuration(1L, SimNaoEnum.NAO, 5.0, 30);

    public static Configuration getInstance() {
        return instance;
    }
}
