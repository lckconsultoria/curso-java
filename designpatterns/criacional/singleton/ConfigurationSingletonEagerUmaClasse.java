package singleton;

// EAGER SINGLETON (ANSIOSO)
// JA OCUPA A MEMORIA DO OBJETO DESDE QUE É CRIADO 
// COM UMA UNICA CLASSE, QUANDO EU ESTOU CONSTRUINDO A CLASSE "Configuration" E TENHO ACESSO A TODOS OS ATRIBUTOS QUE DEVE TER.
// NESTE CASO, POSSO CRIAR JA A CLASSE COMO SENDO UM SINGLETON.
public class ConfigurationSingletonEagerUmaClasse {

    private Long id;
    private SimNaoEnum utilizaVendaPrazo;
    private Double percentualJuros;
    private Integer diasCobrancaJuros;

    private static ConfigurationSingletonEagerUmaClasse instance = new ConfigurationSingletonEagerUmaClasse(1L,SimNaoEnum.NAO,5.0,30);

    
    // utiliza o construtor com parâmetros se quiser ja ter um valor padrao 
    private ConfigurationSingletonEagerUmaClasse(Long id, SimNaoEnum utilizaVendaPrazo, Double percentualJuros,
            Integer diasCobrancaJuros) {
        this.id = id;
        this.utilizaVendaPrazo = utilizaVendaPrazo;
        this.percentualJuros = percentualJuros;
        this.diasCobrancaJuros = diasCobrancaJuros;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SimNaoEnum getUtilizaVendaPrazo() {
        return utilizaVendaPrazo;
    }

    public void setUtilizaVendaPrazo(SimNaoEnum utilizaVendaPrazo) {
        this.utilizaVendaPrazo = utilizaVendaPrazo;
    }

    public Double getPercentualJuros() {
        return percentualJuros;
    }

    public void setPercentualJuros(Double percentualJuros) {
        this.percentualJuros = percentualJuros;
    }

    public Integer getDiasCobrancaJuros() {
        return diasCobrancaJuros;
    }

    public void setDiasCobrancaJuros(Integer diasCobrancaJuros) {
        this.diasCobrancaJuros = diasCobrancaJuros;
    }

    public static ConfigurationSingletonEagerUmaClasse getInstance() {
        return instance;
    }

}
