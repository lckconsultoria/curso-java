package singleton;

// LAZY SINGLETON ( PREGUIÇOSO )
// SÓ OCUPA MEMORIA SE FOR PRECISAR 
// COM 2 CLASSES, NORMALMENTE SE EU NAO TIVESSE ACESSO A CLASSE "Configuration" e precise fazer um singleton dela
public class ConfigurationSingletonLazyDuasClasses {
    private static Configuration instance;

    private ConfigurationSingletonLazyDuasClasses() {
    }

    public static synchronized Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration(1L, SimNaoEnum.SIM, 5.3, 30);
        }
        return instance;
    }
}
