package singleton;

// LAZY SINGLETON ( PREGUIÇOSO )
// SÓ OCUPA MEMORIA SE FOR PRECISAR 
// COM UMA UNICA CLASSE, QUANDO EU ESTOU CONSTRUINDO A CLASSE "Configuration" E TENHO ACESSO A TODOS OS ATRIBUTOS QUE DEVE TER.
// NESTE CASO, POSSO CRIAR JA A CLASSE COMO SENDO UM SINGLETON.
public class ConfigurationSingletonLazyUmaClasse {
    private Long id;
    private SimNaoEnum utilizaVendaPrazo;
    private Double percentualJuros;
    private Integer diasCobrancaJuros;

    private static ConfigurationSingletonLazyUmaClasse instance;

    // com parametros se for precisar de um default
    private ConfigurationSingletonLazyUmaClasse(Long id, SimNaoEnum utilizaVendaPrazo, Double percentualJuros, Integer diasCobrancaJuros) {
        this.id = id;
        this.utilizaVendaPrazo = utilizaVendaPrazo;
        this.percentualJuros = percentualJuros;
        this.diasCobrancaJuros = diasCobrancaJuros;
    }

    public static synchronized ConfigurationSingletonLazyUmaClasse getInstance() {
        if(instance == null) {
            instance = new ConfigurationSingletonLazyUmaClasse(1L,SimNaoEnum.SIM,5.3, 30);
        }
        return instance;
    }

    public Long getId() {
        return id;
    }

    public SimNaoEnum getUtilizaVendaPrazo() {
        return utilizaVendaPrazo;
    }

    public Double getPercentualJuros() {
        return percentualJuros;
    }

    public Integer getDiasCobrancaJuros() {
        return diasCobrancaJuros;
    }
    
}
