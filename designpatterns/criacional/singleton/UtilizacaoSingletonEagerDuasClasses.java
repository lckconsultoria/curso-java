package singleton;

public class UtilizacaoSingletonEagerDuasClasses {
    public static void main(String[] args) {
        Configuration configuration1 = ConfigurationSingletonEagerDuasClasses.getInstance();
        Configuration configuration2 = ConfigurationSingletonEagerDuasClasses.getInstance();
     
        if(configuration1 == configuration2) {
            System.out.println("Mesmo objeto ");
        } else {
            System.out.println("Objetos diferentes ");
        }
    }
}
