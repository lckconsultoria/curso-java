package singleton;

public class UtilizacaoSingletonEagerUmaClasse {
    public static void main(String[] args) {
        ConfigurationSingletonEagerUmaClasse configuration1 = ConfigurationSingletonEagerUmaClasse.getInstance();
        ConfigurationSingletonEagerUmaClasse configuration2 = ConfigurationSingletonEagerUmaClasse.getInstance();
     
        if(configuration1 == configuration2) {
            System.out.println("Mesmo objeto ");
        } else {
            System.out.println("Objetos diferentes ");
        }
    }
}
