package singleton;

public class UtilizacaoSingletonLazyDuasClasses {
    public static void main(String[] args) {
        Configuration config1 = ConfigurationSingletonLazyDuasClasses.getInstance();
        Configuration config2 = ConfigurationSingletonLazyDuasClasses.getInstance();
        config1.setPercentualJuros(50.0);
        System.out.println(config2); // altera os dois 

        if(config1 == config2) {
            System.out.println("Mesmo objeto");
        } else  {
            System.out.println("Objetos diferentes");
        }
    }
}
