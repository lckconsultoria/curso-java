package singleton;

public class UtilizacaoSingletonLazyUmaClasse {
    public static void main(String[] args) {
        ConfigurationSingletonLazyUmaClasse configuration1 = ConfigurationSingletonLazyUmaClasse.getInstance();
        ConfigurationSingletonLazyUmaClasse configuration2 = ConfigurationSingletonLazyUmaClasse.getInstance();
     
        if(configuration1 == configuration2) {
            System.out.println("Mesmo objeto ");
        } else {
            System.out.println("Objetos diferentes ");
        }
    }
}
