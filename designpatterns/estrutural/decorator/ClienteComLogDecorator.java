package decorator;

public class ClienteComLogDecorator implements ClienteInterface {
    private Cliente cliente;

    public ClienteComLogDecorator(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void activate() {
        System.out.println("Antes ativar");
        cliente.activate();
    }
    
    @Override
    public void deactivate() {
        System.out.println("Antes desativar");
        cliente.deactivate();

    }
    
}
