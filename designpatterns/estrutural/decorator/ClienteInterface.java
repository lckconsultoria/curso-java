package decorator;

public interface ClienteInterface {
    void activate();
    void deactivate();
}
