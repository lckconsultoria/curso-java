package decorator;

public class TesteDecorator {
 
    public static void main(String[] args) {
        Cliente cliente = new Cliente();

        ClienteInterface clienteComLogDecorator = new ClienteComLogDecorator(cliente);
        desativa(cliente);
    }

    static void desativa(ClienteInterface cliente) {
        cliente.activate();
    }
}
