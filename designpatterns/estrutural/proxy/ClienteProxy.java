package proxy;

public class ClienteProxy extends Cliente {

    @Override
    public void activate() {
        System.out.println("antes ativar");
        super.activate();
    }
    
    @Override
    public void deactivate() {
        System.out.println("antes inativar");
        // TODO Auto-generated method stub
        super.deactivate();
    }
}
