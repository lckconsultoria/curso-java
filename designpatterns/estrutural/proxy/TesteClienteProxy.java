package proxy;

public class TesteClienteProxy {
    public static void main(String[] args) {
        Cliente cliente = new Cliente();
        Cliente clienteProxy  = new ClienteProxy();
        desativa(clienteProxy);

    }

    static void desativa(Cliente cliente) {
        cliente.activate();
    }
}
