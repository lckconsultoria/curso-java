package proxy.exemplo_boleto;

import java.time.LocalDate;

public class AcbrBoleto {
    public void acrescentaBoleto(double valor, LocalDate vencimento) {
        System.out.println("acbr adicionou item ");
    }

    public void sendRemessa() {
        System.out.println("acbr enviou remessa");
    }

    public void receiveRemessa() {
        System.out.println("acbr recebeu remessa");
    }
}
