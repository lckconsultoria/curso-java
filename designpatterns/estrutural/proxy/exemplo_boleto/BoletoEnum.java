package proxy.exemplo_boleto;

public enum BoletoEnum {
    COBREBEMX(new BoletoProxyCobreBemLck()), ACBR(new BoletoProxyACBRLck());

    private BoletoInterface boleto;

    private BoletoEnum(BoletoInterface boletoInterface) {
        this.boleto = boletoInterface;
    }

    
    public BoletoInterface getImpl() {
        return this.boleto;
    } 
}
