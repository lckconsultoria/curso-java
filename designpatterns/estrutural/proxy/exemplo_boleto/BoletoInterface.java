package proxy.exemplo_boleto;

import java.time.LocalDate;

public interface BoletoInterface {
    void addBoleto(double valor, LocalDate vencimento);
    void enviarRemessa();
    void recebeRetorno();
}
