package proxy.exemplo_boleto;

import java.time.LocalDate;

public class BoletoProxyACBRLck implements BoletoInterface {
    private AcbrBoleto acbrBoleto = new AcbrBoleto();

    @Override
    public void addBoleto(double valor, LocalDate vencimento) {
       acbrBoleto.acrescentaBoleto(valor, vencimento);
    }

    @Override
    public void enviarRemessa() {
        acbrBoleto.sendRemessa();
    }
    
    @Override
    public void recebeRetorno() {
        acbrBoleto.receiveRemessa();
    }
    
}
