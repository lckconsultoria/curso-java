package proxy.exemplo_boleto;

import java.time.LocalDate;

public class BoletoProxyCobreBemLck implements BoletoInterface {
    private CobreBemx cobreBemx = new CobreBemx();

    @Override
    public void addBoleto(double valor, LocalDate vencimento) {
       cobreBemx.addItemReceber(valor, vencimento);
    }

    @Override
    public void enviarRemessa() {
        cobreBemx.enviarArquivoRemesssa();
    }
    
    @Override
    public void recebeRetorno() {
        cobreBemx.receberArquivoRemesssa();
    }
    
}
