package proxy.exemplo_boleto;

import java.time.LocalDate;

public class CobreBemx {
    public void addItemReceber(double valor, LocalDate vencimento) {
        System.out.println(" CobreBEMX adicionou item ");
    }

    public void enviarArquivoRemesssa() {
        System.out.println("CobreBEMX enviou remessa");
    }

    public void receberArquivoRemesssa() {
        System.out.println("CobreBEMX recebeu remessa");
    }
}
