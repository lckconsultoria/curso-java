package proxy.exemplo_boleto;

import java.time.LocalDate;

public class TestaBoleto {
 
    public static void main(String[] args) {
        BoletoInterface boletoCobreBemX = BoletoEnum.valueOf("COBREBEMX").getImpl();
        boletoCobreBemX.addBoleto(10.35,LocalDate.now());
        boletoCobreBemX.enviarRemessa();

        BoletoInterface boletoACBR = new BoletoProxyACBRLck();
        boletoACBR.addBoleto(10.35,LocalDate.now());
        boletoACBR.enviarRemessa();
    }
}
