package proxy.proxy_anonimo;

import java.time.LocalDate;

public interface BoletoInterface {
    void addBoleto(double valor, LocalDate vencimento);
    void enviarRemessa();
    void recebeRetorno();
}
