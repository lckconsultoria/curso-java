package proxy.proxy_anonimo;

import java.time.LocalDate;

import proxy.exemplo_boleto.CobreBemx;

public class EnviaBoletoProxyAnonimo {
    public static void main(String[] args) {
        BoletoInterface boletoCobreBemx = new BoletoInterface() {
            private CobreBemx boleto = new CobreBemx();

            @Override
            public void addBoleto(double valor, LocalDate vencimento) {
                boleto.addItemReceber(valor, vencimento);
            }

            @Override
            public void enviarRemessa() {
                boleto.enviarArquivoRemesssa();
            }

            @Override
            public void recebeRetorno() {
                boleto.receberArquivoRemesssa();
            }
        };
    }
}
