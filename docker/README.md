<center><img src="./images/logo.png"></center>

## Imagem  
Uma imagem do Docker, ou imagem de contêiner, é um arquivo executável independente usado para criar um contêiner. Essa imagem de contêiner contém todas as bibliotecas, dependências e arquivos de que o contêiner precisa para ser executado.   

### Comandos relacionados a imagem  
* docker pull \<nome-da-imagem\>    &rarr; utilizado para baixar a imagem do hub.docker.com 
* docker push \<nome-da-imagem\>    &rarr; sobe a imagem para o hub.docker.com  
* docker images  &rarr; utilizado para listar todas as imagens do seu computador  
* docker rmi  \<nome-da-imagem\> [-f] &rarr;  utilizado para remover uma imagem   e o -f é utilizado para remover imagens em uso   
* docker images -q   &rarr;  lista apenas os *IMAGE ID* da imagem   
  * Nota: os IMAGE ID são hash que identificam apenas essa imagem e podem ser utilizados no lugar do nome da imagem  
* docker rmi $(docker images -q) -f  &rarr;  remove todas as imagens

## Container 
### Comandos relacionados a container   
* *docker run \<nome-da-imagem\>*   &rarr;  cria um container a partir da imagem, baixando-a se necessário  
* Subcomandos 
  * -- name \<nome-do-container\>  &rarr; fixa o nome do container, caso contrário um nome aleatório irá ser criado   
  * -p \<porta-local\>:<\porta-interna-do-container>   &rarr;  expõe a porta interna do container na sua porta local  
  * -d   &rarr;  executa em background (segundo plano)  
  * -v \<pasta-local-do-seu-computador\>:\<pasta-dentro-do-container\>  &rarr; volume - mapeia uma pasta interna do container para uma pasta local do seu computador  
  * -e \<variavel-de-ambiente\>=\<valor-da-variável-de-ambiente\>  &rarr;  cria uma variável de ambiente dentro do container  
  * -it  &rarr;  executa o container de modo interativo, utilizado sempre em conjunto com uma instrução que deve ser executado dentro do container (normalmente bash)  
    * *docker run --name myubuntu -it ubuntu bash*  &rarr;  executa o bash dentro do container myubuntu (baseado na imagem ubuntu) permitindo que se aplique comandos dentro do container
  * exec -it  &rarr; mesma coisa da instrução anterior, porém para containeres já em execução 
    * docker exec -it \<nome-do-container-em-execução\> \<comando\>
    * docker exec -it lck-mysql bash &rarr; executa o bash no container lck-mysql 
* *docker ps* &rarr; lista os containers em execução  
  * subcomandos 
  * -a &rarr; lista todos os containers os em execução e os parados  
* *docker rm \<nome-do-container\>* &rarr; remove um container 
* Subcomandos 
  * -f &rarr; força a remoção mesmo se o container estiver em execução 
* *docker stop \<nome-do-container\>*  &rarr; para a execução de um container   
* *docker start \<nome-do-container\>*  &rarr; starta um container parado   
* *docker logs \nome-do-container\>* &rarr; mostra o log do container 

## Comandos Docker 
* docker login &rarr; faz login no hub.docker.com 
* docker build -t \<nome-da-imagem\> . &rarr; builda o arquivo Dockerfile dentro da pasta (cria uma imagem baseado nos comandos do Dockerfile) 
* docker build -t myimage -f Dockerfile-nginx-lck .  &rarr; builda uma imagem a partir do arquivo Dockerfile-nginx-lck 

## Alguns containers  
docker run --name lck-mysql -e MYSQL_ROOT_PASSWORD=123 -d -p 3310:3306   -v //d//projetos//docker//data:/var/lib/mysql -d mysql  &rarr; sobe o mysql  
docker run --name myserver -p 90:80 -v //d//projetos//docker//html:/usr/share/nginx/html -d nginx  &rarr; sobe o nginx  

## dentro do bash do mysql  
mysql --user=root --password   

## Exemplo de Dockerfile 


```
#  &rarr; comentário 
FROM adoptopenjdk/openjdk14:x86_64-alpine-jre-14.0.2_12 => FROM especifica qual imagem base para a imagem que está sendo construida 

RUN apt update  => o RUN serve para executar comandos dentro da imagem que será construida 
RUN apt install nano -y 

ENV JAVA_HOME=/jdk =>ENV seta uma variável de ambiente dentro do container 

COPY ./lucrosis-api/target/lucrosis-api.jar /usr/app/app.jar   => o COPY serve para copiar arquivos locais para uma determinada pasta dentro do container 

EXPOSE 8080/tcp => expoe a porta tcp 8080 dentro do container. O parametro /protocolo se omitido se entende que é o TCP 

WORKDIR /usr/app/ => WORKDIR seta a pasta inicial do container 

ENTRYPOINT [ "java","-jar","app.jar" ] => ENTRYPOINT executa o comando com os argumentos passados 

ou 

CMD ["java", "-jar", "app.jar"] => CMD faz praticamente o mesmo que o ENTRYPOINT faz, porém o ENTRYPOINT permite que se troque o comando que será executado.

```

### IMPORTANTE:  
CMD ["tail", "-f", "/dev/null"]  &rarr;  serve para parar o container e ficar esperando.


## Arquivo DockerCompose
*docker compose up -d*  &rarr; põe em execução os containers do docker-compose.yml 
*docker compose -f "docker-compose.yml" up -d --build* &rarr; põe em execução os containers do docker-compose em questão   
*docker compose -f "docker-compose.yml" down*  &rarr; tira de execução os container de dentro do  docker-compose em questão   

