package donttellask;

public class Principal {
    public static void main(String[] args) {
        var conta = new Conta();
        // deposito inicial
        conta.setSaldo(1000);

        // realizar saque 
        conta.setSaldo(conta.getSaldo() - 350);

        System.out.println(conta.getSaldo());
    }
}
