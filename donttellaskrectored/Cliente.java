package donttellaskrectored;

public class Cliente {
    private Long id;
    private String name;
    private boolean active;
    
    public Cliente() {
        this.active = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public void Inactivate() {
        this.active = false;
    }
}
