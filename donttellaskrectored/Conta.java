package donttellaskrectored;

public class Conta {
    private double saldo;

    public Conta() {
        this.saldo = 0;
    }

    public double getSaldo() {
        return saldo;
    }

    public double depositar(double valor) {
        return saldo += valor;
    }

    public double sacar(double valor) {
        return saldo -= valor;
    }
}
