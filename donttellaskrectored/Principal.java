package donttellaskrectored;

public class Principal {
    public static void main(String[] args) {
        var conta = new Conta();

        System.out.printf("saldo da conta é %8.2f %n", conta.getSaldo() );
        System.out.printf("saldo após depositar de 1000 é: %8.2f %n",conta.depositar(1000));
        System.out.printf("saldo após sacar 700 é: %8.2f %n",conta.sacar(700));
        System.out.printf("Saldo final: %8.2f",conta.getSaldo());

        var conta2 = new Conta();
        conta2.depositar(3000.00);
        conta2.sacar(500);
        System.out.println(conta2.getSaldo());
    }
}
