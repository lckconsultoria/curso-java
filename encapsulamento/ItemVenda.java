package encapsulamento;
public class ItemVenda {
    private Integer id;
    private Produto produto;
    private Integer quantidade;
    private Double valorVenda;
    
    public ItemVenda(Integer id, Produto produto, Integer quantidade) {
        this.id = id;
        this.produto = produto;
        this.quantidade = quantidade;
        this.valorVenda = produto.getValor();
    }

    public Double getTotalItem() {
        return quantidade * valorVenda;
    }

    // getters / setters / equals / hashCode / toString
        public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Produto getProduto() {
        return produto;
    }
    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    public Integer getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
    public Double getValorVenda() {
        return valorVenda;
    }
    public void setValorVenda(Double valorVenda) {
        this.valorVenda = valorVenda;
    }
}
