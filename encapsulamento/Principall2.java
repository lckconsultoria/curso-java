package encapsulamento;

import java.time.LocalDate;

public class Principall2 {
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente(1L, "Luciano");
        Produto cocaCola = new Produto(1L, "Coca-cola", 3.95);
        Produto pepsiCola = new Produto(2L, "Pepsi cola", 6.22);
        Produto coxinha = new Produto(3L, "Coxinha", 5.00);
        Produto kibe = new Produto(3L, "Kibe", 5.00);

        Venda primeiraVenda = new Venda(1L, LocalDate.now(), cliente1);
        primeiraVenda.adicionarItem(cocaCola, 2);
        System.out.println(primeiraVenda.valorTotalPedido());

        primeiraVenda.adicionarItem(kibe, 1);
        System.out.println(primeiraVenda.valorTotalPedido());

        primeiraVenda.removerItem(2);
        System.out.println(primeiraVenda.valorTotalPedido());
        // quantidade de itens
        System.out.println(primeiraVenda.getItens().size());

        ItemVenda itemVenda = new ItemVenda(5, kibe, 3);
        primeiraVenda.getItens().add(itemVenda);
        System.out.println(primeiraVenda.valorTotalPedido());
        System.out.println(primeiraVenda.getItens().size()); // acrescentou o item 

    }
}
