package encapsulamento;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class VendaRefatorada {
    private Long id;
    private LocalDate dataPedido;
    private Cliente cliente;
    private List<ItemVenda> itens = new ArrayList<>();
    private Double valorTotalPedido;
    private Integer proxItem = 1;
    

    public VendaRefatorada(Long id, LocalDate dataPedido, Cliente cliente) {
        this.id = id;
        this.dataPedido = dataPedido;
        this.cliente = cliente;
    }

    public void adicionarItem(Produto produto, Integer quantidade) {
        itens.add(new ItemVenda(proxItem++,produto, quantidade));
        
        valorTotalPedido = 0D;
        for (ItemVenda itemVenda : itens) {
            valorTotalPedido += itemVenda.getTotalItem();
        }
    }
    
    public void removerItem(Integer idARemover) {
        Iterator<ItemVenda> itensIterator = itens.iterator();
        while(itensIterator.hasNext()) {
            ItemVenda item = itensIterator.next();
            if(item.getId().equals(idARemover)) {
                itensIterator.remove();
                break;
            }
        }
        valorTotalPedido = 0D;
        for (ItemVenda itemVenda : itens) {
            valorTotalPedido += itemVenda.getTotalItem();
        }
    }
    
    public Double valorTotalPedido() {
        return this.valorTotalPedido;
    }
    
    // getters / setters / hashCode / equals / toString 
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public LocalDate getDataPedido() {
        return dataPedido;
    }
    public void setDataPedido(LocalDate dataPedido) {
        this.dataPedido = dataPedido;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public List<ItemVenda> getItens() {
        return new ArrayList<>(this.itens);
    }

    

}
