package entrada_de_dados;
import java.util.Scanner;

public class EntradaDeDados {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Entre seu nome: ");

        String nome = entrada.nextLine();
        System.out.printf("Seu nome é \"%s\"", nome);
    }
}
