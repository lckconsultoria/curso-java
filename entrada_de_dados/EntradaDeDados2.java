package entrada_de_dados;
import java.util.Scanner;

public class EntradaDeDados2 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        
        System.out.print("Entre seu peso: ");
        int peso = entrada.nextInt();
        
        System.out.print("Entre sua altura : ");
        double altura = entrada.nextDouble();

        // depois de um nextDouble ou nextInt se for ter outra coisa 
        entrada.nextLine(); // tem de fazer esse RTP

        System.out.print("Entre seu nome: ");
        String nome = entrada.nextLine();
        
        double imc = peso / (altura * altura);

        System.out.printf("Seu nome é \"%s\" %n", nome);
        System.out.printf("seu imc é %7.2f ", imc);
    }
}
