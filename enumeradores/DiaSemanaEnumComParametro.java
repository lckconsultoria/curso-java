package enumeradores;

public enum DiaSemanaEnumComParametro {
    SEG("Segunda-feira"), //
    TER("Terça-feira"), //
    QUA("Quarta-feira"), //
    QUI("Quinta-feira"), //
    SEX("Sexta-feira"), //
    SAB("Sábado"), //
    DOM("Domingo");

    private final String descricaoLonga;

    private DiaSemanaEnumComParametro(String descricaoLonga) {
        this.descricaoLonga = descricaoLonga;
    }

    public String getDescricaoLonga() {
        return descricaoLonga;
    }
}
