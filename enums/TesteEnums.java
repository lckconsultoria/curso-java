public class TesteEnums {
    public static void main(String[] args) {
        System.out.println(UFEnum.MG.getDescricaoUF());
        System.out.println(UFEnum.RJ.torcida());
        System.out.println(UFEnum.MG.torcida());
        System.out.println(UFEnum.MG.populacao());
        System.out.println(UFEnum.RJ.populacao());

        System.out.println(UFEnum.MG.ordinal());
        System.out.println(UFEnum.MG.name());
        System.out.println(UFEnum.MG);

        System.out.println("*".repeat(30)+" lista todos os values do enum ");
        for (UFEnum myEnum : UFEnum.values()) {
            System.out.println(myEnum.name());
        }

        System.out.println(UFEnum.valueOf("SP").ordinal());
        System.out.println(UFEnum.values()[1]); //pega o segundo elemento do enum
        // System.out.println(UFEnum.valueOf("XX")); // nao funciona pq nao tem 

        listaPorUF(null);
    }

    static void listaPorUF(UFEnum ufEnum) {
        if(UFEnum.SP.equals(ufEnum)) {
            System.out.println("é SP");
            return;
        }
        System.out.println("Diferente de SP");
    } 
}
