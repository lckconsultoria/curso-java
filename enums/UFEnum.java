

public enum UFEnum {
    SP("São Paulo"),
    MG("Minas Gerais") {
        @Override
        public String torcida() {
            return "Atletico mineiro";
        }
    },
    RJ(2000L); 

    private String descricaoUF;
    private Long populacao;

    private UFEnum(Long populacao) {
        this.populacao = 1000L;
    }

    private UFEnum(String descricaoUF) {
        this.descricaoUF = descricaoUF;
    }

    public String getDescricaoUF() {
        return this.descricaoUF;
    }

    public String torcida() {
        return "Palmeiras";
    }

    public Long populacao(){
        return this.populacao;
    }
}
