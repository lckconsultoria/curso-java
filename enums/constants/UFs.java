package constants;

public class UFs {
    private UFs() {}
    public static final String SP = "SP";
    public static final String MG = "MG";
    public static final String RJ = "RJ";

    public static void main(String[] args) {
       listarPorUF(UFs.MG); 
    }

    static void listarPorUF(String uf) {
        System.out.println("Listou da uf "+uf);
    }
}
