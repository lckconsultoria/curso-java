package escopo_de_inicializacao_variaveis;

import java.util.Scanner;

public class EscopoDeInicializacaoDeVariaveis {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Valor da venda  ");
        double valorVenda = entrada.nextDouble();

        System.out.print("Cobrar frete  ");
        Boolean cobrarFrete = entrada.nextBoolean();
        double valorFrete = 10;
        if (cobrarFrete) {
            double valorTotalDaVenda = 15;
            valorTotalDaVenda = valorVenda + valorFrete;
            System.out.println(valorTotalDaVenda);
        }
        // System.out.println(valorTotalDaVenda); // nao compila pois a variável foi
        // criada dentro do bloco do if do cobrar Frete;

    }
}
