# Externalizando propriedades   

## Arquivos properties  
### application.properties
lucrosis.mail.hostName= mail.luck.com  
lucrosis.mail.port = 1234  
lucrosis.mail.from = lucksjb@gmail.com  
luck.cpf = ${LUCK_CPF} 
luck.rg = ${LUCK_RG:22.222.333.44}  
&rarr; O  ${CPF} irá substituir colocar como valor da propriedade luck.cpf o conteúdo da variável de ambiente LUCK_CPF  
&rarr; O  ${LUCK_RG:22.222.333.44}  ter como padrão caso a variável de ambiente LUCK_RG não exista 22.222.333.44
**OBSERVAÇÃO:* na versão atual do spring não é necessário utilizar o ${}, basta definir a variável de ambiente  
 




### Exemplo de Configuração em Record  
```
    @Component
    public record ConfigPropertiesRecord(@Value("${lucrosis.senhabanco}") String senhaBanco) {
        
    }
```

### Exemplo de Configuração em Classe utilizando o ConfigurationProperties 
```
// essa classe tem de ter os Getters and Setters 
@Configuration
@ConfigurationProperties(prefix = "lucrosis.mail")
@Validated  // valida utilizando a JSR-380 (jakarta.validation.constraints.*)
public class ConfigProperties {

    private String hostName;

    @Min(1025)
    @Max(65536)
    private int port;

    @Pattern(regexp = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$")
    private String from;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
```

### Exemplo de utilização  
```
@RestController
@RequestMapping
public class TesteController {
    @Autowired
    private ConfigProperties config;

    @Autowired
    private ConfigPropertiesRecord configPropertiesRecord;

    @Value("${mensagem.imprimir}")
    private String mensagemImprimir;
    
    @GetMapping("/teste")
    public String getMethodName() {
        String text = """
            <p1> %s </p1> <br>

            hostName: %s <br>
            port: %s <br> <br>
            senha do Banco: %s

                """.formatted(mensagemImprimir, config.getHostName(), config.getPort(), configPropertiesRecord.senhaBanco());
        
        // String.format("<p1> %s </p1>", mensagemImprimir);
        return text;
    }
    
    
}
```

### Para evitar que o properties fique "chiando" 
*  Botao direito sobre uma das propriedades que estão chiando ou crie manualmente o arquivo src\main\resources\META-INF\additional-spring-configuration-metadata.json
   *  ![alt text](./images/property-metadata1.png "Botao direito sobre uma das propriedades") <br> <br>
*  Arquivo src\main\resources\META-INF\additional-spring-configuration-metadata.json
   *  ![alt text](./images/property-metadata2.png "Irá criar o arquivo src\main\resources\META-INF\additional-spring-configuration-metadata.json ") <br> <br>
*  Preencha os campos corretamente 
   *  ![alt text](./images/property-metadata3.png "Preencha corretamente o arquivo ")  <br> <br>
*  Isso irá habilitar o autocomplete do spring  
   *  ![alt text](./images/property-metadata4.png "Isso irá habilitar o autocomplete")  <br> <br>
*  E explicações queando parar o mouse sobre a propriedade
   *  ![alt text](./images/property-metadata5.png "E dará uma mensagem de ajuda ao posicionar o mouse em cima ")  <br> <br>
  
<br><br>

### Se for utilizar a validação é necessário a dependência  
```
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

### Passando parametros via command line 
 java -jar -Dluck.cpf=456.789.123.45 nome-do-jar.jar

### Passando parametros por environment variable  
* Crie uma variavel de ambiente no S.O. que estiver trabalhando com o valor 
  * **Importante:** a variável de ambiente tem de ser UPPER_SNAKE_CASE
* No windows pode utilizar o comando set 
  * set LUCK_CPF = 123.456.789.01
  * java -jar nome-do-jar.jar
* No linux ou no git bash 
  * export LUCK_CPF = 123.456.789.01
  * java -jar nome-do-jar.jar

### Um property File para cada ambiente 
* Crie os arquivos com o nome de application-\<ambiente\>.properties 
* Defina no application.properties a tag spring.profiles.active=\<ambiente\>
* Na hora de chamar execute com 
  * java -jar -DSPRING_PROFILES_ACTIVE=\<ambiente\> \<nome-do-jar\>
  * ou se preferir sete a variavel de ambiente SPRING_PROFILES_ACTIVE com o ambiente desejado 
*  

### Arquivo extensão Yaml  
* Tem o mesmo propósito do arquivo de extensão properties, é um modelo bastante utilizado atualmente  
* Pode utilizar esse site para conversão  https://www.javaguides.net/p/properties-to-yaml-converter-online.html  
```
mensagem:
  imprimir: LUCIANO
lucrosis:
  mail:
    hostName: mail.luck.com
    port: 1234
    from: lucksjb@gmail.com
  senhabanco: 123
spring:
  profiles:
    active: dev
```

### Links 
https://www.baeldung.com/configuration-properties-in-spring-boot   
https://www.baeldung.com/spring-value-annotation   
https://www.baeldung.com/spring-boot-properties-env-variables   
https://www.baeldung.com/spring-profiles  