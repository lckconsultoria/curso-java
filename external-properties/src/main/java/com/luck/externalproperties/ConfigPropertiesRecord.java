package com.luck.externalproperties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public record ConfigPropertiesRecord(@Value("${lucrosis.senhabanco}") String senhaBanco) {
    
}
