package com.luck.externalproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExternalPropertiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternalPropertiesApplication.class, args);
	}

}
