package com.luck.externalproperties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;



@PropertySource("classpath:pegando-variavel-ambiente.properties")
@Configuration
@ConfigurationProperties(prefix = "luck")
public class PegandoDaVariavelDeAmbiente {
  
    private String rg;

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
  
}
