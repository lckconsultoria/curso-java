package com.luck.externalproperties;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TesteController {
    @Autowired
    private ConfigProperties config;

    @Autowired
    private ConfigPropertiesRecord configPropertiesRecord;

    @Autowired
    private EscolhendoArquivoProperties escolhendoArquivoProperties;

    
    @Autowired
    private PegandoDaVariavelDeAmbiente pegandoDaVariavelDeAmbiente;

    @Value("${mensagem.imprimir}")
    private String mensagemImprimir;

    @Value("${luck.ambiente}")
    private String ambiente;

    @Autowired
    private TesteProperties authProperties;
    
    @GetMapping("/teste")
    public String getMethodName() {
        String text = """
            <p1> %s </p1> <br>

            hostName: %s <br>
            port: %s <br> <br>
            senha do Banco: %s <br> <br>
            Cpf: %s <br><br>
            RG: %s <br>
            ambiente: %s <br>
            providerUrl: %s

                """.formatted(mensagemImprimir, config.getHostName(), 
                config.getPort(), configPropertiesRecord.senhaBanco(),
                escolhendoArquivoProperties.getCpf(),
                pegandoDaVariavelDeAmbiente.getRg(), 
                ambiente,
                authProperties.getProviderUrl()
                );
        
        // String.format("<p1> %s </p1>", mensagemImprimir);
        return text;
    }
    
    
}
