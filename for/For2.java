public class For2 {
    public static void main(String[] args) {
        int i = 0;
        for (; i < 10; ) {
            i++;
            if ((i % 2) == 0) {
                continue; // volta no inicio do for 
            }
            if (i == 5) {
                break; // abandona o for 
            }
            System.out.println(i);
        }
    }
}
