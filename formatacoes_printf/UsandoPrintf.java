package formatacoes_printf;

public class UsandoPrintf {
    public static void main(String[] args) {
        int id = 10;
        String nome = "Luciano";
        float salario = 12_333.23f;
        System.out.printf("id: %d, nome: \"%s\", valor: %010.2f%n ", id, nome, salario);
        System.out.println("id " + id + " nome " + nome + " salario " + salario);
        System.out.println("C:\\lucrosis");
        String nomeEntreAspas = "\"Marcio \"";
        String pasta = "c:\\lucrosis"; // escapa a /
        System.out.printf("A comissao é de %5.2f%%%n ", 13.5f);
        float comissao = 2.23f;
        String formatada = String
                .format("O id %d (%s) tem o salario de %7.2f com a comissão %5.2f%%",
                        id, nome, salario, comissao);
        System.out.println(formatada);
    }
}
