package extendendointerface;

public class GenericPermitindoApenasUmtipoDeInterface<T extends Number> {
    T number;

    void guarda(T number) {
        this.number = number;
    }

    T recupera() {
        return this.number;
    }
}
