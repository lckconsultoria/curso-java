package genericInterface;

public interface GenericCRUD<T, K> {
    T incluir(T t);

    T alterar(T t);
    
    void apagar(K key);

    T buscar(K key);

    void listar();
}
