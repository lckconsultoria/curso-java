package minhalista;

public class DemoCuringa {
    public static void main(String[] args) {
        MinhaListaGeneric<String> minhaListaGeneric = new MinhaListaGeneric<>(String.class);
        minhaListaGeneric.add("Luciano");
        minhaListaGeneric.add("Gabriel");
        retirarTodos(minhaListaGeneric);

        MinhaListaGeneric<Cliente> clientes = new MinhaListaGeneric<>(Cliente.class);

        var cli1  = new Cliente();
        cli1.id = 1;
        cli1.nome = "Luciano";
        clientes.add(cli1);

        var cli2  = new Cliente();
        cli2.id = 2;
        cli2.nome = "Gabriel";
        clientes.add(cli2);
        retirarTodos(clientes);
    }

    static void  retirarTodos(MinhaListaGeneric<?> lista) {
      while(lista.size()>0) {
          lista.remove(0);
      }
    }

    static double media(MinhaListaGeneric<? extends Number> numeros ) {
        var total = 0.0;
        for (int i = 0; i < numeros.size(); i++) {
          total += numeros.get(i).doubleValue();  
        }
        return total / numeros.size();
    }
}
