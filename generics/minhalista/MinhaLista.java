package minhalista;
import java.util.Arrays;

public class MinhaLista {
    String[] objects = new String[0];

    void add(String value) {
        String[] tmpObjects = new String[objects.length + 1];
        for (int i = 0; i < objects.length; i++) {
            tmpObjects[i] = objects[i];
        }
        tmpObjects[objects.length] = value;
        objects = tmpObjects;
        tmpObjects = null;
    }

    void remove(int index) {
        if (index < 0 || index > objects.length - 1) {
            System.out.println("indice inválido, tem de ser entre 0 e " + (objects.length - 1));
        }
        String[] tmpObjects = new String[objects.length - 1];
        int newIndex = 0;
        for (int i = 0; i < objects.length; i++) {
            if (i == index) {
                continue;
            }
            tmpObjects[newIndex++] = objects[i];
        }
        objects = tmpObjects;
        tmpObjects = null;
    }

    String get(int i) {
        return objects[i];
    }

    String[] get() {
        return objects;
    }

    int size() {
        return objects.length;
    }

    void list() {
        for (String string : objects) {
            System.out.println(string);
        }
    }

    void listOrdered() {
        String[] tmpOrdered = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            tmpOrdered[i] = objects[i];
        }
        Arrays.sort(tmpOrdered);
        for (String object : tmpOrdered) {
            System.out.println(object);
        }
    }
}
