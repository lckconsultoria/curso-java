package minhalista;

import java.lang.reflect.Array;

public class MinhaListaGeneric<T> {
    T[] objects;
    private final Class<T> tipoClasse;

    public MinhaListaGeneric(Class<T> tipoClasse) {
        this.tipoClasse = tipoClasse;
        objects = (T[]) Array.newInstance(tipoClasse, 0);
    }

    void add(T value) {
        T[] tmpObjects = (T[]) Array.newInstance(tipoClasse, objects.length + 1);
        for (int i = 0; i < objects.length; i++) {
            tmpObjects[i] = objects[i];
        }
        tmpObjects[objects.length] = value;
        objects = tmpObjects;
        tmpObjects = null;
    }

    void remove(int index) {
        if (index < 0 || index > objects.length - 1) {
            System.out.println("indice inválido, tem de ser entre 0 e " + (objects.length
                    - 1));
        }
        T[] tmpObjects = (T[]) Array.newInstance(tipoClasse, objects.length - 1);
        int newIndex = 0;
        for (int i = 0; i < objects.length; i++) {
            if (i == index) {
                continue;
            }
            tmpObjects[newIndex++] = objects[i];
        }
        objects = tmpObjects;
        tmpObjects = null;
    }

    T get(int i) {
        return objects[i];
    }

    T[] get() {
        return objects;
    }

    int size() {
        return objects.length;
    }

    void list() {
        for (T object : objects) {
            System.out.println(object);
        }
    }
}
