package minhalista;
public class TesteMinhaLista {
    public static void main(String[] args) {
        MinhaLista minhaLista = new MinhaLista();
        minhaLista.list();
        minhaLista.add("Luciano");
        minhaLista.add("Marta");
        minhaLista.add("Felipe");
        minhaLista.add("Joao");
        minhaLista.add("Yuri");
        minhaLista.list();

        System.out.println("****** LISTOU COM GET(i) *******");
        for (int i = 0; i < minhaLista.size(); i++) {
            System.out.println(minhaLista.get(i));
        }
        
        System.out.println("****** LISTOU COM ENHANCED FOR *******");
        for (String string : minhaLista.get()) {
            System.out.println(string);
        }
        
        System.out.println("****** REMOVEU PRIMEIRO ELEMENTO *******");
        minhaLista.remove(0);
        for (String string : minhaLista.get()) {
            System.out.println(string);
        }

        System.out.println("****** REMOVEU TERCEIRO ELEMENTO *******");
        minhaLista.remove(3);
        for (String string : minhaLista.get()) {
            System.out.println(string);
        }
        
        System.out.println("****** List Ordenado *******");
        minhaLista.listOrdered();
        
        System.out.println("****** List sem Ordem *******");
        minhaLista.list();
    }
}
