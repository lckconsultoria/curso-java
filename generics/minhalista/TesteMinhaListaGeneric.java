package minhalista;

public class TesteMinhaListaGeneric {
    public static void main(String[] args) {

        // lista de Integer
        MinhaListaGeneric<Integer> listaInteger = new MinhaListaGeneric<>(Integer.class);
        listaInteger.add(10);
        listaInteger.add(5);
        listaInteger.add(3);
        listaInteger.add(24);
        listaInteger.add(16);

        listaInteger.remove(3);

        Integer integerPosicao1daLista = listaInteger.get(1);
        System.out.println(integerPosicao1daLista);

        Integer[] lista =  listaInteger.get();

        System.out.println(listaInteger.size());

        listaInteger.list();

        // lista de Clientes
        MinhaListaGeneric<Cliente> listaClientes = new MinhaListaGeneric<>(Cliente.class);
        var cliente1 = new Cliente();
        cliente1.id = 10;
        cliente1.nome = "Luciano";

        var cliente2 = new Cliente();
        cliente2.id = 10;
        cliente2.nome = "Luciano";

        listaClientes.add(cliente1);
        listaClientes.add(cliente2);

        listaInteger.remove(3);

        Cliente clientePosicao1daLista = listaClientes.get(1);
        System.out.println(clientePosicao1daLista);

        Cliente[] listaCliente = (Cliente[]) listaClientes.get();

        System.out.println(listaClientes.size());

        listaClientes.list();
    }
}
