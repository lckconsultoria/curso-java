import java.util.Random;

public class SorteadorGeneric {
    private static final Random RAMDOM = new Random();

    static <T> T sortear(T[] objects) {
        if(objects.length == 0) {
            throw new IllegalArgumentException("Tem de ter ao menos um elemento");
        }

        int idx = RAMDOM.nextInt(objects.length);
        return objects[idx];
    }
}