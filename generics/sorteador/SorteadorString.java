import java.util.Random;

public class SorteadorString {
    private static final Random RAMDOM = new Random();

    static String sortear(String[] strings) {
        if(strings.length == 0) {
            throw new IllegalArgumentException("Tem de ter ao menos um elemento");
        }

        int idx = RAMDOM.nextInt(strings.length);
        return strings[idx];
    }
}