public class TestaGenericClass {
    public static void main(String[] args) {
        GenericClass<Cliente> genericClass = new GenericClass<>();
        genericClass.guardar(new Cliente(10L,"Luciano "));
        Cliente clienteResgatado = genericClass.resgatar();
        System.out.println(clienteResgatado);
    }
}
