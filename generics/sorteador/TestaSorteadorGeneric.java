public class TestaSorteadorGeneric {
    public static void main(String[] args) {

        var clientes = new Cliente[] { //
                new Cliente(1L, "Luciano"), //
                new Cliente(2L, "Jéssica"), //
                new Cliente(3L, "João"), //
                new Cliente(4L, "André"), //
                new Cliente(5L, "Gabriel"), //
                new Cliente(6L, "Robson") //
        };
        Cliente clienteSorteado = SorteadorGeneric.<Cliente>sortear(clientes);
        System.out.println(clienteSorteado);
    }
}
