package heranca;

public class TesteHeranca {
    public static void main(String[] args) {
        Conta conta = new Conta();
        ContaEspecial contaEspecial = new ContaEspecial();
        
        contaEspecial = (ContaEspecial) conta;
        // conta = contaEspecial;
        System.out.println(conta.getClass().getName()); // ContaEspecial
        System.out.println(contaEspecial.getClass().getName()); // ContaEspecial
    }
}
