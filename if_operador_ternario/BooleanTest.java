package if_operador_ternario;

import java.util.Scanner;

public class BooleanTest {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Entre seu peso: ");
        int peso = entrada.nextInt();

        System.out.print("Entre sua idade: ");
        int idade = entrada.nextInt();

        boolean pesoNaoOK = peso > 90;
        boolean idadeNaoOk = !(idade <= 100);

        if (pesoNaoOK || idadeNaoOk) {
            System.out.println("Tome cuidado seu peso ou sua idade estao altos");
        } else {
            System.out.println("não precisa se preocupar");
        }

    }
}
