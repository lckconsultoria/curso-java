package if_operador_ternario;

import java.util.Scanner;

public class If {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Entre seu peso: ");
        int peso = entrada.nextInt();

        if (peso > 90 && peso <= 200) {
            System.out.println("gordo");
        } else if (peso > 200) {
            System.out.println(" Extra gordo");
        } else {
            System.out.println("Em forma ");
        }
    }
}
