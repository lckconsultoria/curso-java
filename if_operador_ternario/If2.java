package if_operador_ternario;

import java.util.Scanner;

public class If2 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Entre seu peso: ");
        int peso = entrada.nextInt();

        System.out.print("Entre sua idade: ");
        int idade = entrada.nextInt();

        if (peso > 90 || idade > 70) {
            System.out.println("Tome cuidado sua idade ou peso ja estao altos ");
        } else {
            System.out.println("Tudo bem com você ");
        }
    }
}
