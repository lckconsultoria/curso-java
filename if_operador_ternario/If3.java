package if_operador_ternario;

public class If3 {
    public static void main(String[] args) {
        int a = 0;
        int b = 11;
        if (a++ == 0 || b-- == 10) {
            System.out.printf("entrou a: %d  b:  %d %n", a, b);
        }

        if (--a == 0 && ++b == 10) {
            System.out.printf("entrou a: %d  b:  %d %n", a, b);
        }

        if ((a = a + 3) > 2) { // executa o a = a+3 e o resultado faz a comparaçao
            System.out.printf("entrou a: %d %n", a);
        }

        System.out.println("a " + a);
        System.out.println("b " + b);
    }
}
