package if_operador_ternario;
import java.util.Scanner;

public class OperadorTernario {
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Mes:  ");
        int  mes = entrada.nextInt();
        String mesOkString = (mes >= 1 && mes <= 12)? "mes correto " : "mes incorreto";
        boolean mesOkBoolean = (mes >= 1 && mes <= 12)? true : false;
        boolean mesOkBoolean2 = (mes >= 1 && mes <= 12);
        int mesOkInt = (mes >= 1 && mes <= 6)? 1 : 0;
        String parteDoAno = mesOkInt == 1?"primeiro semestre":"segundo semestre";
        System.out.println(parteDoAno);
        System.out.println(mesOkString); 

    }
}
