package if_operador_ternario;

public class if4 {
    public static void main(String[] args) {
        int a = 0;
        int b = 10;
        int c = 50;
        int d = 60;

        /*
         * or
         * true + true = true
         * true + false = true
         * false + true = true
         * false + false = false
         * 
         * and
         * true + true = true
         * true + false = false
         * false + true = false
         * false + false = false
         */

        // if(a == 0 && b == 10 || c == 50 && d != 60) {
        if ((a == 0 && b == 10) || (c == 50 && d != 60)) { // boas práticas colocar entre parenteses
            System.out.println("entrou ");
        }

        boolean resultadoPrimeiroE = a == 0 && b == 10;
        boolean resultadoSegundoE = c == 50 && d != 60;
        System.out.println(resultadoPrimeiroE);
        System.out.println(resultadoSegundoE);
        boolean resultadoOr = resultadoPrimeiroE || resultadoSegundoE;
        System.out.println(resultadoOr);

    }
}
