package interface_aula.calculo;

public interface CalculoAdicionalInterface {
    double calculaDeNovo(double a, double b);
}
