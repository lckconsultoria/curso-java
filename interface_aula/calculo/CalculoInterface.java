package interface_aula.calculo;

public interface CalculoInterface {
    // nao é uma boa pratica 
    // static final Integer constante =10  ;
    
    double calcula(double valor1, double valor2);

    default void imprimir() {
      System.out.println("Alo mundo ");
    }


    // não é boa pratica metodos static em interface
    // static void bada() {
    //     System.out.println("askldjfkasjl");
    // }
}
