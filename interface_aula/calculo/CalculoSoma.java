package interface_aula.calculo;

public class CalculoSoma implements CalculoInterface, CalculoAdicionalInterface {
    long id;
    String nome;

    @Override
    public double calcula(double valor1, double valor2) {
        return valor1+valor2;
    }

    @Override
    public double calculaDeNovo(double a, double b) {
        
       return 10;
    }
}
