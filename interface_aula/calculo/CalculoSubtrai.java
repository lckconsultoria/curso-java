package interface_aula.calculo;

public class CalculoSubtrai implements CalculoInterface{

    @Override
    public double calcula(double valor1, double valor2) {
        return valor1-valor2;
    }
}
