package interface_aula.calculo;

public class TestaInterface {
    public static void main(String[] args) {
        CalculoInterface soma = new CalculoSoma();
        var result = soma.calcula(1.0, 5.0);
        System.out.println(result);
        
        CalculoInterface subtrai = new CalculoSubtrai();
        result = subtrai.calcula(1.0, 5.0);
        System.out.println(result);
        soma.imprimir();
        subtrai.imprimir();
        imprimir(soma, 10, 2);

    }

    static void imprimir(CalculoInterface calculo, double v1, double v2) {
        System.out.println(calculo.calcula(v1, v2));
    }


}
