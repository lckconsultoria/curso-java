package interface_aula.docfiscais;

public class NFE implements DocFiscalInterface, DocInterfaceGeral {
    private Long id;

    @Override
    public double calcular() {
        return 0.0;
    }

    @Override
    public void enviar() {
        System.out.println("Enviou a nota ");
    }

    @Override
    public String gerarXML() {
        return "<xml> <bada> teset </bada> </xml>";
    }

    @Override
    public void cancelar() {
       System.out.println("cancelou no sefaz");
    }

    @Override
    public void imprimir() {
        System.out.println("imprimiu ");
    }

    public void inativarNumeracao() {
        System.out.println("inativado");
    }
}
