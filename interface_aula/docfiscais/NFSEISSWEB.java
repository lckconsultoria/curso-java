package interface_aula.docfiscais;

public class NFSEISSWEB implements NFSInterface {

    @Override
    public void substituir(String nro) {
       System.out.println("Substituiu "+nro);
    }

    @Override
    public double calcular() {
      return 10.0;
    }

    @Override
    public void enviar() {
      System.out.println("Enviado para prefeitura ");
    }

    @Override
    public String gerarXML() {
        return "<xml><tag> bugada </tag> </xml>";
    }

    @Override
    public void cancelar() {
       System.out.println("Cancelado na prefeitura");
    }

    @Override
    public void imprimir() {
       System.out.println("imprimiu ");
    }

    @Override
    public double calculaISS() {
        return 15.0;
    }
}
