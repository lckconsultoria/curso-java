package interface_aula.docfiscais;

public class OsService {
    public void imprimeNota(NFSInterface nfs) {
        nfs.calcular();
        nfs.calculaISS();
        nfs.gerarXML();
        nfs.imprimir();
    }
}
