package interfaces;

public class CalculaComSoma implements MetodoDefaultInterface {

    @Override
    public double calcula(Integer a, Integer b) {
        return a+b;
    }
    
}
