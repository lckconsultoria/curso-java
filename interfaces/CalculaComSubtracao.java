package interfaces;

public class CalculaComSubtracao implements MetodoDefaultInterface {

    @Override
    public double calcula(Integer a, Integer b) {
        return a-b;
    }
    
}
