package interfaces;

public interface MetodoDefaultInterface {
    double calcula(Integer a, Integer b);

    default double calculaMedia(Integer a, Integer b) {
        return calcula(a, b)/2;
    }
    
} 