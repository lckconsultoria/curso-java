package interfaces;

public class NotificadorClienteSMS  implements NotificadorClienteInterface{

    @Override
    public void enviaMensagem(String mensagem) {
        System.out.println("Enviou mensagem por SMS "+mensagem);
    }
}
