package interfaces;

public class NotificadorClienteWhatsApp  implements NotificadorClienteInterface{

    @Override
    public void enviaMensagem(String mensagem) {
        System.out.println("Enviou mensagem por WhatsApp "+mensagem);
    }
}
