package leidemetrio;
public class Principal {
    public static void main(String[] args) {
        Cidade cidade = new Cidade();
        cidade.id = 1;
        cidade.nome = "Sao Joaquim da Barra";
        cidade.uf = "SP";

        Fabricante fabricante = new Fabricante();
        fabricante.id  = 10;
        fabricante.nome = "BMW";
        fabricante.cidade = cidade;

        Carro meuCarro = new Carro();
        meuCarro.id = 10;
        meuCarro.modelo = "BMW Série 1";
        meuCarro.anoFabricacao = 2021;
        meuCarro.cor = "Preto";
        meuCarro.fabricante = fabricante;
        System.out.println(meuCarro.getCidadeFabricante());
    }
}
