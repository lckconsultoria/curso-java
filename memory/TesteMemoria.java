package memory;

/* Para executar esse programa precisa configurar a memoria Heap 
 java -Xmx12G TestePerformance.java
 Configurando a memória heap
Importante definir pois há um custo de processamento para alocação de memória em runtime, quem faz essa alocação é o Garbage collector.

-Xmx<qtd><unid. medida → G(gb), M(mb)> ex: -Xmx900M → define o  máximo de memória da JVM (Runtime.getRuntime().maxMemory()) para 900 Mb
-Xms<qtd><unid. medida → G(gb), M(mb)> ex: -Xmx400M → define a memória heap que já será reservado (Runtime.getRuntime().totalMemory()) para 400 Mb
Runtime.getRuntime().maxMemory() → máximo de bytes de memória heap que a JVM pode utilizar
Runtime.getRuntime().totalMemory() → total de memória heap que a JVM já está reservando 
(não está em uso ainda, mas já está alocada)
Runtime.getRuntime().freeMemory() → total de memória heap que a JVM LIVRE do que já foi reservado  (totalMemory)
(memória realmente utilizada = totalMemory - freeMemory)
 */
public class TesteMemoria {
    static final int KB = 256; //int 4 bytes (1024/4)
    static final int MB = KB * 1024;
    static final int GB = MB * 1024;

    public static void main(String[] args) {
        int alocar = 0;
        String unidadeAlocacao = "MB";
        if (args.length != 2) {
            System.out.println("Usage Java -XmxConfiguration TesteMemoria 1 MB (com espaço entre a quantidade e unidade)\n");
            if (unidadeAlocacao != "KB" && unidadeAlocacao != "MB" && unidadeAlocacao != "GB") {
                System.out.println("Unidade de alocação tem de ser KB ou MB ou GB");
            }
            System.out.println("Para configurar a memória Máxima que pode ser utilizada pela JVM");
            System.out.println("java -Xmx<qtd><unid. medida → G(gb), M(mb)> ");
            System.out.println(
                    "ex: -Xmx900M → define o  máximo de memória heap (Runtime.getRuntime().maxMemory()) para 900 Mb");

            System.out.println("Para configurar a memória já reservada que pode ser utilizada pela JVM");
            System.out.println("java -Xms<qtd><unid. medida → G(gb), M(mb)> --> Runtime.getRuntime().freeMemory()");
            System.out.println(
                    "ex: java -Xmx6g  -Xms1g TesteMemoria.java 100 MB");
                    
            return;
        }
        alocar = Integer.parseInt(args[0]);
        unidadeAlocacao = args[1];

        System.out.println("Memoria alocada para jvm (maxMemory): " + emMegabytes(Runtime.getRuntime().maxMemory()));
        System.out.printf("Memória já reservada pela jvm (totalMemory) %s%n ", emMegabytes(Runtime.getRuntime().totalMemory()));
        System.out.printf("Memória disponível  (freeMemory) %s%n ",
                emMegabytes(Runtime.getRuntime().freeMemory()));

        long memoriaUsadaDaReservada = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Memoria Já utilizada da reservada (totalMemory - freeMemory) " + emMegabytes(memoriaUsadaDaReservada));

        int[] intArray;
        if (unidadeAlocacao.equals("KB")) {
            intArray = new int[KB * alocar];
        }
        if (unidadeAlocacao.equals("MB")) {
            intArray = new int[MB * alocar];
        }
        if (unidadeAlocacao.equals("GB")) {
            intArray = new int[GB * alocar];
        }

        System.out.printf("==> APOS ALOCAR %d %s %n", alocar, unidadeAlocacao);
        System.out.println("Memoria alocada para jvm (maxMemory) : " + emMegabytes(Runtime.getRuntime().maxMemory()));
        System.out.printf("Memória já reservada pela jvm  (totalMemory) %s%n ", emMegabytes(Runtime.getRuntime().totalMemory()));

        System.out.printf("Memória disponível (freeMemory)  %s%n ",
                emMegabytes(Runtime.getRuntime().freeMemory()));

        memoriaUsadaDaReservada = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Memoria Já utilizada da reservada (totalMemory - freeMemory) " + emMegabytes(memoriaUsadaDaReservada));
    }

    static String emMegabytes(long bytes) {
        return String.format("%.2fMB", bytes / 1024d / 1024d);
    }
}
