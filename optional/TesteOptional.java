package optional;

import java.util.Optional;

public class TesteOptional {
    static Segmento seg;

    public static void main(String[] args) {
        Optional<Integer> meuOptional = Optional.empty();
        meuOptional = Optional.of(10);
        meuOptional = Optional.ofNullable(null);
        if (!meuOptional.isPresent()) {
            System.out.println("vazio ");
        } else {
            System.out.println(meuOptional.get());
        }

        System.out.println("*".repeat(30));
        Segmento supermercado = new Segmento(1L, "Supermercado");

        Optional<Segmento> segmentoOptional = Optional.ofNullable(supermercado);
        Optional<Segmento> segOptional2 = segmentoOptional.filter((s) -> s.id == 1);
        segOptional2.ifPresentOrElse((s) -> System.out.println("filter = " + s.id + "-" + s.name),
                () -> System.out.println("filter - Nao presente"));

        Segmento seg2 = segmentoOptional.orElse(new Segmento(2L, "mercearia"));
        System.out.println(seg2.name);

        Segmento seg3 = segmentoOptional.orElseGet(() -> {
            return new Segmento(10L, "casa de carne");
        });
        System.out.println(seg3);

        Segmento seg4 = segmentoOptional.orElseThrow(() -> new RuntimeException("Erro nao existe"));
        System.out.println("orElseThrow " + seg4);

        // segmentoOptional.ifPresent((seg) -> System.out.println(seg.name));

        // Consumer<Segmento> presente = (s) -> {
        // System.out.println("processa presente ");
        // System.out.println(s.id + "- "+ s.name);
        // };

        // segmentoOptional.ifPresentOrElse(presente, () -> System.out.println("**nao
        // presente"));

        if (segmentoOptional.isPresent()) {
            System.out.println(segmentoOptional.get());
        } else {
            System.out.println("nao presente");
        }

    }

}

class Segmento {
    long id;
    String name;

    public Segmento(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Segmento [id=" + id + ", name=" + name + "]";
    }

}