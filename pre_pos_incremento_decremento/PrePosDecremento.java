package pre_pos_incremento_decremento;

public class PrePosDecremento {
    public static void main(String[] args) {
        // pre decremento => decremento depois atribui
        int a = 10;
        int b = --a;

        // mesmo que:
        // a = a-1;
        // int b = a;

        System.out.println(a);
        System.out.println(b);

        // pos decremento => atribui depois decremento
        int c = 10;
        int d = c--;
        // mesmo que:
        // int d = c;
        // c = c -1
        System.out.println(c);
        System.out.println(d);
    }
}
