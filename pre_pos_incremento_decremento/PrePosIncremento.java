package pre_pos_incremento_decremento;
public class PrePosIncremento {
    public static void main(String[] args) {
        // pre incremento => incrementa depois atribui
        int a = 10;
        int b = ++a;
        
        // mesmo que:
        // a = a+1;
        // int b = a;
        
        System.out.println(a);
        System.out.println(b);
        
        // pos incremento => atribui depois incrementa
        int c = 10;
        int d = c++;
        // mesmo que:
        // int d = c;
        // c = c +1
        System.out.println(c);
        System.out.println(d);

    }
}
