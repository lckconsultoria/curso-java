package src.br.com.lckconsultoria.lucrosis.models;

public class ProdutoSemEcapsulamento {
    Integer id;
    String descricao;
    
    @Override
    public String toString() {
        return "Produto [id=" + id + ", descricao=" + descricao + "]";
    }

    
}
