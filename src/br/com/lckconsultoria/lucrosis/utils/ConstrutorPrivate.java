package src.br.com.lckconsultoria.lucrosis.utils;
public class ConstrutorPrivate {

    private ConstrutorPrivate() {
    }

    public static double calculaMedia(double v1, double v2) {
        return (v1 + v2) / 2;
    }
}
