package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TesteStream {
    public static void main(String[] args) {
        List<Segmento> segmentos = new ArrayList<>(
                Arrays.asList(new Segmento(1L, "Mercado"),
                        new Segmento(2L, "Açougue"),
                        new Segmento(3L, "Mercearia"),
                        new Segmento(4L, "Conveniencia"),
                        new Segmento(5L, "PetShop"),
                        new Segmento(6L, "Mecanica")));

        System.out.println("* Lista todos *");
        segmentos.forEach((seg) -> System.out.println(seg));

        System.out.println("* Lista só os > 4");
        segmentos.parallelStream().filter((s) -> s.id > 4).forEach((s) -> System.out.println(s));

        // for (Segmento segmento : segmentos) {
        // if(segmento.id > 4 ) {
        // System.out.println(segmento);
        // }
        // }
    }
}

class Segmento {
    long id;
    String name;

    public Segmento() {
    }

    public Segmento(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Segmento [id=" + id + ", name=" + name + "]";
    }

}