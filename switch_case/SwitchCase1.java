package switch_case;

import java.util.Scanner;

public class SwitchCase1 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("com a nota (inteiro) ");
        int nota = entrada.nextInt();

        switch (nota) {
            case 1, 2, 3: // 1 or 2 or 3
                System.out.println("ruim");
                break;

            case 4: // 4 or 5 or 6
            case 5:
            case 6:
                System.out.println("regular ");
                break;

            default:
                System.out.println("!= 1,2,3,4,5,6");
                break;
        }
        System.out.println("chegou aqui ");
    }
}
