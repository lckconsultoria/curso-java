package switch_case;

import java.util.Scanner;

public class SwitchCase2 {
    // switch expression
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Dia da semana  ");
        String diaSemana = entrada.nextLine();
        String horario;
        switch (diaSemana) {
            case "seg" -> horario = "fechado";
            case "ter" -> horario = "08:00 as 18:00";
            case "qua" -> horario = "08:00 as 18:00";
            case "qui", "sex" -> {
                horario = "08:00 as 22:00";
                System.out.println("sexto bebe");
            }

            default -> horario = "dia inválido ";
        }
        System.out.println(horario);
    }
}
