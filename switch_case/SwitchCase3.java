package switch_case;

import java.util.Scanner;

public class SwitchCase3 {
    // switch expression
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Dia da semana  ");
        String diaSemana = entrada.nextLine();
        System.out.print("Mes  ");
        int mes = entrada.nextInt();

        String horario = switch (diaSemana) {
            case "seg" -> {
                if (mes == 12) {
                    yield "08:00 as 11:59"; // yeld nao faz parte do if e sim do switch case expression
                }

                yield "fechado";
            }
            case "ter" -> "08:00 as 18:00";
            case "qua" -> "08:00 as 18:00";
            case "qui", "sex" -> "08:00 as 22:00";
            default -> "dia inválido ";
        };
        System.out.println(horario);
    }
}
