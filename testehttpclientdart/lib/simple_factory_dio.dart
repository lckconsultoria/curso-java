import 'package:dio/dio.dart';
import 'package:lck_dio_interceptors/lck_dio_interceptors.dart';

class SimpleFactoryDio {
  static Dio getDio() {
    BaseOptions options = BaseOptions(
        baseUrl: 'http://localhost:8080',
        receiveDataWhenStatusError: true,
        connectTimeout: Duration(milliseconds: 5000),
        receiveTimeout: Duration(milliseconds: 5000),
        responseType: ResponseType.json,
        headers: {
          'Authorization':
              'Bearer ${LckClientCredentialsTokenInterceptor.accessToken}'
        });
    var interceptor = LckClientCredentialsTokenInterceptor(
        baseApiURL: 'http://localhost:8080',
        clientCredentialKey: 'bHVjcm9zaXMtYmZmOmJhY2tlbmQxMjM=');
    List<Interceptor>? interceptors = [interceptor];
    Dio dio = Dio(options);
    dio.interceptors.addAll(interceptors);
    return dio;
  }
}
