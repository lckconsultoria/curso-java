import 'package:testehttpclientdart/simple_factory_dio.dart';

void main(List<String> args) async {
  var dio = SimpleFactoryDio.getDio();

  dio.get('http://localhost:8080/api/private/grupos/1').then((resposta) {
    print(resposta.data);
  });
}
