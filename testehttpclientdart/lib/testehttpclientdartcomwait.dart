import 'package:dio/dio.dart';

void main(List<String> args) async {
  Response response = await getToken();

  BaseOptions options = BaseOptions(
    baseUrl: 'http://localhost:8080',
    connectTimeout: const Duration(milliseconds: 5000),
    receiveTimeout: const Duration(milliseconds: 5000),
    responseType: ResponseType.json,
    headers: {
      'Authorization': 'Bearer ${response.data['access_token']}',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  );

  Dio dio = Dio(options);
  Response grupo = await dio.get('http://localhost:8080/api/private/grupos/1');
  print(grupo);
}

Future<Response> getToken() async {
  BaseOptions options = BaseOptions(
    baseUrl: 'http://localhost:8080',
    connectTimeout: const Duration(milliseconds: 5000),
    receiveTimeout: const Duration(milliseconds: 5000),
    responseType: ResponseType.json,
    headers: {
      'Authorization': 'Basic bHVjcm9zaXMtYmZmOmJhY2tlbmQxMjM=',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  );
  Dio dio = Dio(options);
  Response token = await dio.post('/oauth2/token',
      data: {'grant_type': 'client_credentials', 'scope': 'READ WRITE'});
  return token;
}
