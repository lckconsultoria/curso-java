package tipos_primitivos_operadores;
public class DoubleToInt {
    public static void main(String[] args) {
        double largura = 100.37;
        // int tamanho = largura; // não compila pois double 64 bits e int 32 bits;
        int tamanho = (int) largura; // tem de ter certeza que o que tem dentro de largura cabe
        System.out.println(largura);
        System.out.println(tamanho); // perder as decimais 

        double largura2 = 2_150_000_000.37;
        // int tamanho = largura; // não compila pois double 64 bits e int 32 bits;
        int tamanho2 = (int) largura2; // vai dar outro valor totalmente diferente
        System.out.println(largura2);
        System.out.println(tamanho2);




    }
}
