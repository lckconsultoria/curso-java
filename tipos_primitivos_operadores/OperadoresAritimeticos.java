package tipos_primitivos_operadores;
public class OperadoresAritimeticos {
    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        int c;
        
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%3);

        if((a % 2) == 0) {
            System.out.println("A Par");
        } else {
            System.out.println("A impar");
        }

        if((b % 2) == 0) {
            System.out.println("B Par");
        } else {
            System.out.println("B impar");
        }
    }
}