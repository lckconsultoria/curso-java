package tipos_primitivos_operadores;
public class PromocaoAritimetica {
    public static void main(String[] args) {
        float a = 267676.25F;
        double c = a;

        long l = (long) a;

        long n1 = 1232L;
        int n2 = 1;

        long n3 = n1*n2;

        float pi = 3.14151632f;

        float novo = n3 * pi;

        int i1 = 10;
        int i2 = 3;
        float r1 = i1/i2; // 
        float r2 = i1/(i2 * 1.0f); // tem de uma das partes ser float para nao perder a precisão.
        // float r2 = i1/((float)i2 ); // mesmo que 
        System.out.println(r1 + " - "+r2);

    }
}
