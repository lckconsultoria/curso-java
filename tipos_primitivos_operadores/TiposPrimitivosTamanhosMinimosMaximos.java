package tipos_primitivos_operadores;
public class TiposPrimitivosTamanhosMinimosMaximos {
    public static void main(String[] args) {
        boolean menorValor = false;
        boolean maiorValor = true;

        // 8 bits com sinal -(2^(8-1)) até (2^(8-1))-1
        byte byteMenor = -128;
        byte byteMaior = 127;

        // 16 bits sem sinal 0 até (2^(16))-1
        char menorByte = 0;
        char maiorByte = 65_535;

        // 16 bits com sinal -(2^(16-1)) até (2^(16-1))-1
        short shortMenor = -32_768; // 32 mil
        short shortMaior = 32_767;

        // 32 bits com sinal ()
        int intMenor = -2_147_483_648; // 2 bilhoes
        int intMaior = 2_147_483_647;

        // 64 bits com sinal
        long longMenor = -9_223_372_036_854_775_808L; // 9 quintilhoes
        long longMaior = 9_223_372_036_854_775_807L;

        // float 
        // 32 bits, mas o menor e maior valor depende da precisão
        
        // double 
        // 64 bits, mas o menor e maior valor depende da precisão
        
        
    }
}
