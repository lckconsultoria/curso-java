package tipos_wrapper;

public class CastTiposWrapper {
    public static void main(String[] args) {
        Integer integer1 = 10; 
        // Long long1 = integer1; // nao compila
        // Long long1 = (Long) integer1; // nao compila
        Long long1 = integer1.longValue();
        Double double1 = integer1.doubleValue();
        Integer integer2 = double1.intValue();

        Float float1 = integer1.floatValue();

    }
}
