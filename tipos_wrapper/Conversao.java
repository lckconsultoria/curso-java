package tipos_wrapper;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Conversao {

    public static void main(String[] args) {
        // String[][][] empresasFuncionarios = {
        // {
        // { "andre", "gabriel", "joao" },
        // { "Marcio", "Felipinho", "Andresa", "Jose carlos" },
        // { "bada bada " }
        // },
        // {
        // { "1", "2", },
        // { "1", "2x", "3", "4", "5" }
        // }
        // };

        // Integer[][][] empresasFuncionarios = {
        //         {
        //                 { 1, 2, 3 },
        //                 { 1, 2, 3 }
        //         },
        //         {
        //                 { 1, 2, 3,1,7,8 }
        //         }
        // };
        // // System.out.println(empresasFuncionarios.length);
        // // System.out.println(empresasFuncionarios[0].length);
        // System.out.println(Arrays.toString(empresasFuncionarios[0][1]));
        // Arrays.sort(empresasFuncionarios[0][1]);

        // System.out.println(Arrays.toString(empresasFuncionarios[0][1]));
        // Arrays.sort(empresasFuncionarios[0][1], Comparator.reverseOrder());
        // System.out.println(Arrays.toString(empresasFuncionarios[0][1]));

        // System.out.println(empresasFuncionarios[1][1][1]);

        // // String[] funcionariosLck = { "andre", "gabriel", "joao" };
        // String[] funcionariosLck = empresasFuncionarios[0];

        // // String[] funcionarioWikiData = { "Marcio", "Felipinho", "andresa", "jose
        // // carlos" };
        // // for (int i = 0; i < funcionarioWikiData.length; i++) {
        // // System.out.println(funcionarioWikiData[i]);
        // // }

        // // String[] funcionarioWikiData = empresasFuncionarios[1];
        // // System.out.println(empresasFuncionarios[1].length);

        // // System.out.println(empresasFuncionarios[0]);

        // System.out.println(Arrays.toString(empresasFuncionarios));
        // System.out.println("******");
        // for (int empresa = 0; empresa < empresasFuncionarios.length; empresa++) {
        // System.out.printf("empresa %d %n", empresa);
        // for (int funcionario = 0; funcionario < empresasFuncionarios[empresa].length;
        // funcionario++) {
        // System.out.printf("Funcionario %d = %s %n", funcionario,
        // empresasFuncionarios[empresa][funcionario]);
        // }
        // }

        // imprimeNomes("joao", "jose", "maria", "antonio");
        // String tmp[] = new String[4];
        // tmp[0] = "joao";
        // tmp[0+1] = jose;
        // tmp[0+2] = maria;
        // tmp[0+3] = antonio;

        // imprimeNomes("ze maria ");
        imprimeNomes("", new String[]{"joao", "jose"});
    }

    static void imprimeNomes(String nomeObrigatorio, String... outrosNomes) {
        // Objects.requireNonNull(nomeObrigatorio);
        // adicionar um elemento no inicio do array 
        String  arrayTmp[] = new String[outrosNomes.length+1];
        arrayTmp[0] = nomeObrigatorio;
        for (int i = 0; i < outrosNomes.length; i++) {
            arrayTmp[i+1] = outrosNomes[i];
        }
        outrosNomes = arrayTmp;
        // 

        // System.out.println(Arrays.toString(arrayTmp));
        for (String nome : outrosNomes) {
            System.out.println(nome);
        }
    }
}
