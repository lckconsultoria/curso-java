package tipos_wrapper;

public class TiposWrapper {
    public static void main(String[] args) {
        Boolean boolean1 =  Boolean.valueOf("true");
        Boolean boolean2 =  Boolean.parseBoolean("true");
        Boolean boolean3 =  Boolean.TRUE;
        Boolean boolean4 =  Boolean.valueOf(true);
        Boolean boolean5 =  true; // auto-boxing 

        Byte byte1 = Byte.valueOf("65");
        Byte byte2 = Byte.parseByte("65");
        Byte byte3 = 65; // auto-boxing 

        Integer integerx = Integer.valueOf(10);
        Integer integer2 = Integer.parseInt("10");
        Integer integer3 = 10; // auto-boxing 


        Long long1 = Long.valueOf(10);
        Long long2 = Long.valueOf("10");
        Long long3 = Long.parseLong("10");
        Long long4 = 10L; // auto-boxing 

        
        Float float1 = Float.valueOf(10.0F);
        Float float2 = Float.valueOf("3.36");
        Float float3 = Float.parseFloat("3.36");
        Float float4 = 2.37F; // auto-boxing 


        Double double1 = Double.valueOf(10.0);
        Double double2 = Double.valueOf("3.36");
        Double double3 = Double.parseDouble("3.36");
        Double double4 = 2.37; // auto-boxing 


        // Short short1 = Short.valueOf
        short short1 = 10;
        Short short2 = Short.valueOf(short1);
        Short short3 =  Short.valueOf("10");
        Short short4 =  Short.parseShort("10");
        Short short5 =  10; // auto-boxing 
        System.out.println("Criou todos os objetos  ");
    }
}
