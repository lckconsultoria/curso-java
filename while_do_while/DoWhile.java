package while_do_while;

public class DoWhile {
    public static void main(String[] args) {
        // do while ... executa ao menos uma vez 
        int i = 0;
        do {
            System.out.println("i: "+i);
            i ++;
        } while(i < 10 );
    }
    
}