package while_do_while;

public class While3 {
    public static void main(String[] args) {
        int i = 0;
        while(i < 100) {
            if((i %2) == 0) { // descarta os pares 
                i++;
                continue;
            }

            if(i > 50) {
                break; // abandona o while 
            }
            System.out.println(i);
            i++;
        }
    }
}
